### Source code of to reproduce the results of the manuscript "Systematic analysis of alterations in the ubiquitin proteolysis system reveals its contribution to driver mutations in cancer" 
### URL to manuscript: https://www.nature.com/articles/s43018-019-0001-2
### URL to preprint: https://www.biorxiv.org/content/10.1101/507764v1

#### It also includes intermediate data, unprocessed external input data, unprocessed internal input data and all raw figures (check [downloads](https://bitbucket.org/bbglab/analysis-degradation/downloads/) from Analysis degradation repository). 

#### Getting data and pre-processing

[preprocessing.ipynb](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/preprocessing.ipynb): This notebook includes the preprocessing steps to download the source data. All the links to external data are also included in the manuscript methods or Supplementary Note. 


#### Figure1: degron analysis and random forest classifier

The notebooks in the [degrons](https://bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/) directory contain all the source code to reproduce the analysis. The numbering of the notebooks illustrates the order of execution. 

Particularly, the panels of Figure 1 can be found in notebooks:

[Figure 1a-b and Extended Data Figure 1a-b](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/3.calculate_properties_degrons_annotated.ipynb): Analysis of annotated degron properties


[Figure 1c and Extended Data Figure 1c-j](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/4.create_RFC_classifier.ipynb): Random Forest Classifier performance evaluation


[Figure 1d and Extended Data Figure 1k-m](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/5.proteome_wide_identification.ipynb): Proteome-wide analysis of biochemical properties of predicted degrons

[Extended data Figure 2](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/5.proteome_wide_identification.ipynb): Proteome-wide analysis distribution of degron probability per degron motif

#### Figure2: effect of missense mutations and in-frame indels in protein stability


[Figure 2a-f](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/TCGA_data/combine_data_sources/Figure2abc_regressions_boxplots.ipynb): Effect of missense mutations in CCNE1, TP53 and CHEK2 mRNA-protein regressions in UCEC, COREAD and UCEC respectively. 

[Figure 2g-h](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/TCGA_data/combine_data_sources/proteome_wide_Stability_Change.ipynb): Proteome-wide effect of amino-acid change across 210 proteins. 

##### Figure3: effect of missense mutations and in-frame indels in predicted and annotated degrons

[Figure 3a](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/needle_plot_NFE2L2.ipynb): Distribution of missense mutations and in-frame indels of NFE2L2 across TCGA samples

[Extended Data Figure 3a](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/needle_plot_CTNNB1.ipynb): Distribution of missense mutations and in-frame indels of CTNNB1 across TCGA samples

[Figure 3b](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/NFE2L2_Boxplot.ipynb): Effect of KEAP1 degrons mutations in NFE2L2

[Figure 3c](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/CTNNB1_Boxplot.ipynb): Effect of BTRC degron mutations in CTNNB1

[Figure 3d](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/CCND1_Boxplot.ipynb): Effect of FOX031 degron mutations in CCND1

[Figure 3e-f and Extended Data Figure 3e](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/FIgure3def_boxplots_tcga_CNA1.ipynb): Effect of mutations in predicted and annotated degrons

[Figure 3g and Extended Data Figure 3j](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/FIgure3def_boxplots_VAF_CNA1.ipynb): Effect of degron mutations according to the mutation VAF

[Figure 3h](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/FIgure3def_boxplots_tcga_CNA1-oncogenes.ipynb): Effect of degron mutations according to the mode of action

[Extended Data Figure 3c-d](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/FIgure3def_boxplots_tcga_CNA0.ipynb): Effect of mutations in diploid genes within predicted and annotated degrons

[Extended Data Figure 3f-h](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/FIgure3def_boxplots_ccle_CNA1.ipynb): Effect of mutations in  genes within predicted and annotated degrons in the CCLE dataset

[Extended Data Figure 3i](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/plots_effect_mutations/FIgure3def_boxplots_tcga_CNA1-ms.ipynb): Effect of mutations in  genes within predicted and annotated degrons in the MS dataset


##### Figure4: de-novo degrons

[Figure 4e](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/de_novo_degrons/3.visualize_all_denovo_degrons.ipynb): scatter plot representation of mutations in de-novo degrons

[Figure 4f-g and Extended Data Figure4c-f](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/de_novo_degrons/prepare_needle_plots_denovo.ipynb): Degron-linkeness profile of examples alongside the Stability Change of mutations along their protein sequence. 

[Extended Data Figure4a-b](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/de_novo_degrons/prepare_needle_plots_degron_like.ipynb): Validation of the de-novo identification strategy

##### Figure5: Positive selection analysis across predicted degrons

[Figure 5b](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/compare_degFIscore_vs_residual.ipynb): Comparison of DegFI between mutations with high versus low degron probability

[Figure 5c](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/SMDeg_TCGA_penta.ipynb): SMdeg results for the TCGA dataset across predited degrons

[Figure 5d](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/FMDeg_TCGA_penta.ipynb): FMdeg results for the TCGA dataset across predited degrons

[Figure 5e](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/degrons_worcloud.ipynb): Wordcloud of driver degrons in TCGA cohorts

[Figure 5f](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/define_driver_degrons.ipynb): Overlap of driver degrons with Cancer Gene Census

[Figure 5g](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/plot_needle_plot_MYCN.ipynb): Needle Plot of MYCN pan-cancer mutations across TCGA cohorts

[Figure 5h](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/plot_needle_plot_CCND1.ipynb): Needle Plot of CCND1 pan-cancer mutations across TCGA cohohts

[Figure 5i](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/plot_needle_plot_PCF11.ipynb): Needle Plot of PCF11 pan-cancer mutations across TCGA cohorts

[Extended Data Figure 5a-b ](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/generate_QQplots.ipynb): QQPlots of FMDeg and SMDeg of the TCGA PAN-cancer analysis

[Extended Data Figure 5c](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/SMDeg_CCLE_penta.ipynb): SMdeg results for the CCLE dataset across predited degrons

[Extended Data Figure 5d](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/FMDeg_CCLE_penta.ipynb): FMdeg results for the CCLE dataset across predited degrons

[Extended Data Figure 5e](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/SMDeg_TCGA_denovo_penta.ipynb): SMdeg results for the TCGA dataset across de-novo degrons

[Extended Data Figure 5f](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/plot_needle_plot_ETV5_CCLE.ipynb): Needle Plot of ETV5 mutations across CCLE cell-lines

[Extended Data Figure 5g](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/plot_needle_plot_CCND3_CCLE.ipynb): Needle Plot of CCND3 mutations across CCLE cell-lines

[Extended Data Figure 5h](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/degrons/positive_selection/analysis/plot_needle_plot_USP36_CCLE.ipynb): Needle Plot of USP36 mutations across CCLE cell-lines

##### Figure6: E3 ubiquitin ligases

[Figure 6a](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/plot_E3_ligases_driver_TCGA.ipynb): Driver E3 ubiquitin ligases according to dNdScv and OncodriveFML across TCGA cohorts

[Figure 6b](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/plot_E3_box_plots_individual_pairs_TCGA.ipynb): Individual boxplots of substrates protein stability change upon mutation in E3 ubiquitin ligases across TCGA samples

[Figure 6c](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/plot_E3_ligases_subtrates_changes_TCGA.ipynb): Volcano plot of substrates protein stability change upon mutation in E3 ubiquitin ligases across TCGA samples

[Figure 6d](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/plot_E3_ligases_subtrates_changes_CCLE.ipynb): Volcano plot of substrates protein stability change upon mutation in E3 ubiquitin ligases across CCLE samples
 
[Extended Data Figure 6a](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/plot_E3_ligases_driver_CCLE.ipynb): Driver E3 ubiquitin ligases according to dNdScv and OncodriveFML across CCLE cohorts

[Extended Data Figure 6b](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/E3_ligases_worcloud.ipynb): Wordcloud of driver E3 ligases across TCGA cohorts

[Extended Data Figure 6c](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/compare_drivers_E3.ipynb): Comparison of driver E3 ligases with other reported datasets of driver ubiquitinases

##### Figure7: driver mutations in the UPS system

[Figure 7b](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/CCNE1_example/CCNE1_plots.ipynb): CCNE1 alterations and their protein expression across TCGA samples

[Figure 7d-e](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/driver_mutations/excess_E3_ligases/calculate_e3_ligases_excess_mutations.ipynb): Driver mutations per sample in E3 ubiquitin ligases across TCGA cohorts

[Figure 7f-g](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/driver_mutations/excess_degrons/calculate_excess_drivers_degrons.ipynb): Driver mutations per samples in predicted degrons across TCGA cohorts

[Figure 7h](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/driver_mutations/general_UPS/calculate_excess_drivers_and_ups.ipynb): Ratio of driver mutations involved in UPS across TCGA cohorts

[Figure 7i](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/TCGA_data/combine_data_sources/proteome_wide_Stability_Change.ipynb): Oncoproteins with significant increase in Stability Change upon missense mutations in the TCGA dataset

[Figure 7j](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/acctionability/perform_acctionability.ipynb): Percentage of TCGA samples with potential benefit from oncogenes with altered E3 upstream ligases 

[Extended Data Figure 7a](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/E3_ligases/analysis/acctionability/perform_acctionability.ipynb):  Percentage of TCGA samples with potential benefit from samples with altered E3 upstream ligases of CCNE1

[Extended Data Figure 7b](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/analysis-degradation/raw/master/driver_mutations/general_UPS/calculate_excess_drivers_and_ups_nocgc.ipynb): Ratio of driver mutations involved in UPS across TCGA cohorts in non CGC genes



