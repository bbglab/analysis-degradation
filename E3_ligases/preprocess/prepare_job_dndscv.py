import os
import glob
import re

dir_base = "/workspace/projects/ubiquitins/codereview/degradation/"
dataset = "tcga"
kmer = "tri"
dir_mutations = os.path.join(dir_base,"data","mutations_e3",dataset)
path_output = os.path.join(dir_base,"data","output_methods_selection","output_dndscv_e3ligases_paper"+"_"+dataset+"_"+kmer+"/")
f = open(os.path.join(dir_base,"data","output_methods_selection","commands_dndscv_e3ligases_"+dataset+"_"+kmer+".sh"),'w')
path_script = os.path.join(dir_base,"create_dataframes","E3_ligases","code","dndscv.R")
'''
f.write("[pre]\n")
f.write('. "/home/$USER/miniconda3/etc/profile.d/conda.sh"\n')
f.write('conda activate smdeg \n')
f.write("[params]\n")
cores = "12"
memory = "32G"
f.write(f"cores = {cores}\n")
f.write(f"memory = {memory}\n")
f.write("[jobs]\n")
'''
f.write("source activate tcga_assembler"+"\n")
print ("Reading data from: "+dir_mutations+"/*_dndscv.tsv.gz")
if not(os.path.exists(path_output)):
    os.mkdir(path_output)
for file in glob.glob(dir_mutations+"/*_dndscv.tsv.gz"):

    m = re.search('([A-Z]+)_dndscv.tsv.gz', file)
    if m:
        cancer_type = m.group(1)
        output1 = os.path.join(path_output,cancer_type+".out.gz")
        output2 = os.path.join(path_output,cancer_type+".annotmuts.out.gz")
        output3 = os.path.join(path_output,cancer_type+".genemuts.out.gz")
        f.write ("Rscript "+ path_script+" "+file +" "+output1+" "+output2+" "+output3 + "\n")

f.close()

