import sys
import glob
import re
import os
import pandas as pd

dir_base = "/workspace/projects/ubiquitins/codereview/degradation"
run = "tcga"
dict_targets = os.path.join(dir_base,'data', 'dict_all_pairs.json' )



if run == "tcga":
    path_file_rppa =os.path.join(dir_base,"data/rppa_matched_irls.tsv.gz")
    rppa = pd.read_csv(path_file_rppa,sep="\t",compression="gzip")
    df_samples_e3_mutated = os.path.join(dir_base,
                                         "data/e3_mutated_grouped.csv")
    ttypes=list(rppa["Cancer_Type"].unique())+["PAN"]
else:
    path_file_rppa = os.path.join(dir_base, "data/rppa_matched_irls_ccle.tsv.gz")
    df_samples_e3_mutated = os.path.join(dir_base,
                                         "data/e3_mutated_grouped_ccle.tsv")
    ttypes = ["PAN"]

path_output = os.path.join(dir_base,"data","output_e3_ligases_change_stability")
path_script = os.path.join(dir_base,"create_dataframes","E3_ligases","code")
# Open the job files
f = open(os.path.join(path_script,"commands_e3_ligases_"+run+".sh"),'w')
# Conda env
f.write('source activate regression \n')
# For each ttype generate a job
for ttype in ttypes:
    file_output = os.path.join(path_output,ttype+"_"+run+".tsv.gz")

    if ttype == "PAN":
        min_muts = 20
    else:
        min_muts = 10
    f.write ( "python " + path_script+"/calculate_differences_upone3_mutation_pan.py measure_change --dict_targets "+dict_targets+" --path_mutations_e3 "+df_samples_e3_mutated
             +" --path_file_rppa "+path_file_rppa+" --cancer_type "+ttype+" --file_output "+ file_output+" --min_muts  "+str(min_muts) +" \n")

f.close()

