import os
import glob
import re

dir_base = "/workspace/projects/ubiquitins/codereview/degradation/"
dataset = "tcga"
kmer = "tri"
dir_mutations = os.path.join(dir_base,"data","mutations_e3",dataset)
path_signatures = os.path.join(dir_base,"external","signatures",kmer)
path_regions = os.path.join(dir_base,"external","cds.regions.tsv.gz")
path_output = os.path.join(dir_base,"data","output_methods_selection","output_oncodrivefml_e3ligases_paper"+"_"+dataset+"_"+kmer+"/")
f = open(os.path.join(dir_base,"data","output_methods_selection","commands_oncodrivefml_e3ligases_"+dataset+"_"+kmer+".sh"),'w')
f.write("[pre]\n")
f.write('. "/home/$USER/miniconda3/etc/profile.d/conda.sh"\n')
f.write('conda activate smdeg \n')
f.write("[params]\n")
cores = "12"
memory = "32G"
f.write(f"cores = {cores}\n")
f.write(f"memory = {memory}\n")
f.write("[jobs]\n")
print ("Reading data from: "+dir_mutations+"/*.tsv.gz")
for file in glob.glob(dir_mutations+"/*.tsv.gz"):

    m = re.search('([A-Z]+).tsv.gz', file)
    if m:
        cancer_type = m.group(1)
        f.write ("oncodrivefml -i "+file+" -o "+path_output+" -e "+path_regions + " --signature "
                 +os.path.join(path_signatures,cancer_type+".json")+ " -t coding -s wes "+ " \n")

f.close()

