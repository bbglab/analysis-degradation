import pandas as pd
from scipy import stats
import numpy as np
import sys
import statsmodels.stats.multitest as mt
import json
import gzip
import click
from tqdm import tqdm
import itertools


def concat(grp):
    l = []
    for group_samples in grp:
        for sample in group_samples.split(","):
            l.append(sample)
    return ",".join(l)

@click.group()
def cli():
    pass

@cli.command(context_settings={'help_option_names': ['-h', '--help'], 'max_content_width': 200})
@click.option('--dict_targets', help='Dictionary of allowed interactions between the E3 ligases and their substrates')
@click.option('--path_mutations_e3', help='Dataframe of the samples with mutations for each E3 ligase')
@click.option('--path_file_rppa', help='Path to the dataframe with the RPPA information')
@click.option('--cancer_type', help='whether to perform an analysis per cancer type or PAN cancer. Type PAN for pan-cancer analysis')
@click.option('--file_output', help='output file where to write the output')
@click.option('--min_muts', help='Minimum number of mutations to perform the test',default=20)
def measure_change(dict_targets,path_mutations_e3,path_file_rppa,cancer_type,file_output,min_muts):

    # Read the input data
    dict_targets = json.load(open(dict_targets,'r'))
    df_diff_info = pd.read_csv(path_file_rppa,sep="\t",compression="gzip")
    df_samples_e3_mutated = pd.read_csv(path_mutations_e3,sep="\t")
    list_results = []
    if cancer_type == "PAN":
        df_diff_info_clean = df_diff_info[
            ((df_diff_info["Phenotype"] == "WT")) & (df_diff_info["CNA"] >= -1) & (df_diff_info["CNA"] <= 1) & (
                np.isfinite(df_diff_info["Stability_Change"]))]
        # Select E3 ligases with more than min_muts mutations
        df_samples_e3_mutated = df_samples_e3_mutated.groupby(["Hugo_Symbol"],as_index=False).agg({"COUNT_MUTS":np.nansum,"Matchable_Sample_ID":concat})
        df_samples_e3_mutated = df_samples_e3_mutated[
            (df_samples_e3_mutated["COUNT_MUTS"] >= min_muts)]

    else:
        df_diff_info_clean = df_diff_info[
            ((df_diff_info["Phenotype"] == "WT")) & (df_diff_info["CNA"] >= -1) & (df_diff_info["CNA"] <= 1) & (
                np.isfinite(df_diff_info["Stability_Change"]))&(df_diff_info["Cancer_Type"]==cancer_type)]
        # Select E3 ligases with more than min_muts mutations
        df_samples_e3_mutated = df_samples_e3_mutated[
            (df_samples_e3_mutated["COUNT_MUTS"] >= min_muts)&(df_samples_e3_mutated["Cancer_Type"]==cancer_type)]

    # For each E3 ligase with enough mutations iterate
    for index, row in tqdm(df_samples_e3_mutated.iterrows(), total=df_samples_e3_mutated.shape[0]):
        e3 = row["Hugo_Symbol"]
        print ("Analyzing "+e3 +" in "+ cancer_type)
        if not(e3) in dict_targets:
            print ("Not targets found for  " + e3)
            continue
        for hugo_target in dict_targets[e3]:
            # Filter the likely targets
            df_diff_info_query = df_diff_info_clean[df_diff_info_clean["Hugo_Symbol"]==hugo_target]
            # get the difference in abundance
            l = get_significant_abudance_distance(row["Hugo_Symbol"], hugo_target, df_diff_info_query, df_samples_e3_mutated,min_muts, cancer_type)
            # Append the results
            list_results = list_results + l

    df_results =  pd.DataFrame(list_results,
                        columns=["Protein_Driver_Mutated", "Cancer_Type", "Protein", "Hugo_Symbol", "P_value",
                                 "Type_Change", "Mean_MT", "Mean_WT", "Len_MT", "Len_WT"])
    df_results["Q_value"] = mt.fdrcorrection(df_results["P_value"], alpha=0.05)[1]
    df_results.to_csv(file_output,sep="\t",compression="gzip",index=False)


def get_significant_abudance_distance(protein_mutated, target, df_rrpa_cancer, df_samples_e3_mutated,min_num, cancer_type):
    '''
    Get the changes in Stability induced by mutations in the E3 ligase protein mutated
    :param protein_mutated:
    :param df_rrpa_cancer:
    :return:
    '''
    samples_rppa = set(df_rrpa_cancer["Matchable_Sample_ID"].unique())
    samples_partial = df_samples_e3_mutated[
        (df_samples_e3_mutated["Hugo_Symbol"] == protein_mutated)][
        "Matchable_Sample_ID"].values
    samples_drivers = samples_partial[0].split(",")

    if len(samples_drivers) < min_num or len(set(samples_drivers).intersection(samples_rppa)) < min_num:
        return [] # Less than 20 points
    value_y = "Stability_Change"
    results = []
    # Iterate over each antibody of the target
    for rppa_protein in set(
            df_rrpa_cancer[(df_rrpa_cancer["Matchable_Sample_ID"].isin(samples_drivers))]["Protein"].values):


        df_rrpa_cancer_mutated = df_rrpa_cancer[(df_rrpa_cancer["Matchable_Sample_ID"].isin(samples_drivers)) & (
                df_rrpa_cancer["Protein"] == rppa_protein) & (np.isfinite(df_rrpa_cancer[value_y]))&(df_rrpa_cancer["Hugo_Symbol"]==target)][
            ["Matchable_Sample_ID", "Protein", value_y]]

        val_mutated = df_rrpa_cancer_mutated[value_y].values
        if (len(val_mutated) < min_num):
            continue # Maybe there are not enough samples of this antibody

        df_rrpa_cancer_not_mutated = df_rrpa_cancer[~(df_rrpa_cancer["Matchable_Sample_ID"].isin(samples_drivers)) & (
                df_rrpa_cancer["Protein"] == rppa_protein) & (np.isfinite(df_rrpa_cancer[value_y]))][
            ["Matchable_Sample_ID", "Protein", value_y]]
        val_wt = df_rrpa_cancer_not_mutated[value_y].values
        t, pval = stats.mannwhitneyu(val_mutated, val_wt, alternative='two-sided')
        # We check the type of change and annotate it
        type_change = "decrease"
        if np.nanmean(val_mutated) > np.nanmean(val_wt):
            type_change = "increase"

        results.append([protein_mutated,cancer_type , rppa_protein,
                        target,pval, type_change, np.nanmean(val_mutated), np.nanmean(val_wt), len(val_mutated), len(val_wt)])
    return results


if __name__ == '__main__':
    cli()

