import pandas as pd
import os
import re
import glob
import pickle
import numpy as np
from find_sorrounding_phospho import find_sorrounding_phospho
from find_sorrounding_lysines import find_sorrounding_lysines

'''
Paths to fetch the properties

'''

path_base = "../data"
path_base_sequences = os.path.join(path_base,"sequences")
path_base_sequences_msa = os.path.join(path_base,"sequences","MSA")
path_base_sequences_msa_scores = os.path.join(path_base,"sequences","MSA","Scores/")
path_base_features = os.path.join(path_base,"features/")
path_base_features_asa = os.path.join(path_base_features,"ASA/")
path_base_features_anchor = os.path.join(path_base_features,"ANCHOR/")
path_base_features_dss = os.path.join(path_base_features,"DSS/")
path_base_features_flex = os.path.join(path_base_features,"FLEX/")
path_default_simulated = "/home/fran/Documents/ubiquitin/code/prepare_submission/create_dataframes/degrons/data/simulated_degrons.csv.gz" # Default path, not necessary except for simulated degrons
path_pfam = "../external/PFAM_data.tsv"


def get_cons(row):
    '''
    Return the conservation of the degron (average of amino acids)
    :param row:
    :return:
    '''
    file_f = "/home/fran/Documents/ubiquitin/data/dregons/positive_selection/MSA/Scores/" + row["Entry"] + ".score"

    try:
        df_entry = pd.read_csv(file_f, sep="\t", comment='#', skiprows=1, names=["RES_NUM_OLD", "RES", "CONS"])
        df_entry["RES_NUM"] = range(1, df_entry.shape[0] + 1)
    except:
        return np.nan
    return np.nanmean(
        list(df_entry[(df_entry["RES_NUM"] >= row["START"]) & (df_entry["RES_NUM"] <= row["END"])]["CONS"].values))


def get_cons_flanking(row, flanking_pos=11):
    '''
    Calculate the flanking conservation of a degron compared to N flanking pos
    :param row: the dataframe row
    :return:
    '''
    file_f =  path_base_sequences_msa_scores + row["Entry"] + ".score"
    try:
        df_entry = pd.read_csv(file_f, sep="\t", comment='#', skiprows=1, names=["RES_NUM_OLD", "RES", "CONS"])
        df_entry["RES_NUM"] = range(1, df_entry.shape[0] + 1)
    except: # File not found
        return np.nan
    degron = np.nanmean(
        list(df_entry[(df_entry["RES_NUM"] >= row["START"]) & (df_entry["RES_NUM"] <= row["END"])]["CONS"].values)) # Calculate the mean conservation of the degron

    flanking = np.nanmean(list(df_entry[((df_entry["RES_NUM"] >= row["START"] - flanking_pos) & (
            df_entry["RES_NUM"] < row["START"])) | ((df_entry["RES_NUM"] > row["END"]) & (
            df_entry["RES_NUM"] <= row["END"] + flanking_pos))]["CONS"].values)) # Compare with the mean conservation of the flanking pos

    return (degron / flanking)


def get_dss(row):
    '''
    Calculate the mean DSS of the degron
    :param row:
    :return:
    '''
    file_f = path_base_features_dss + row["Entry_Isoform"] + ".fa"

    try:
        df_entry = pd.read_csv(file_f, sep="\t", comment='#', names=["RES_NUM", "RES", "DSS"])
    except:
        return np.nan
    return np.nanmean(
        list(df_entry[(df_entry["RES_NUM"] >= row["START"]) & (df_entry["RES_NUM"] <= row["END"])]["DSS"].values))



def get_anchor(row):
    '''
    Return the average ANCHORING score of the degron
    :param row:
    :return:
    '''
    file_f = path_base_features_anchor + row["Entry_Isoform"] + ".fa.out"

    try:
        df_entry = pd.read_csv(file_f, sep="\t", comment='#', names=["RES_NUM", "RES", "ANCHOR", "OUT"])
    except:
        return np.nan
    return np.nanmean(
        list(df_entry[(df_entry["RES_NUM"] >= row["START"]) & (df_entry["RES_NUM"] <= row["END"])]["ANCHOR"].values))



def get_flex_protein(row):
    '''
    Return the average Rigidity of the degron
    :param uniprot_isoform:
    :return:
    '''

    file_f = path_base_features_flex + row["Entry_Isoform"] + '_backbone.pred'

    try:
        df_entry = pd.read_csv(file_f, sep="\t", skiprows=11, names=["RES", "FLEX"])
    except:

        return np.nan
    df_entry["Entry_Isoform"] = row["Entry_Isoform"]
    df_entry["FLEX"].fillna(0.0,inplace=True)
    df_entry["RES_NUM"] = range(1, df_entry.shape[0] + 1)
    rig = np.nanmean(list(df_entry[(df_entry["Entry_Isoform"] == row["Entry_Isoform"]) & (
            df_entry["RES_NUM"] >= row["START"]) & (df_entry["RES_NUM"] <= row["END"])]["FLEX"].values))
    return rig


def get_asa_protein(row):
    '''
    Returns the average ASA of the degron
    :param uniprot_isoform:
    :return:
    '''
    try:

        file_f = path_base_features_asa + row["Entry_Isoform"] + ".spd3"
        df_ss = pd.read_csv(file_f, sep="\t", skiprows=1,
                               names=["RES_NUM", "RES", "SS", "ASA", "Phi", "Psi", "Theta(i-1=>i+1)", "Tau(i-2=>i+1)",
                                      "P(C)", "P(E)", "P(H)"])
        df_ss["Entry_Isoform"] = row["Entry_Isoform"]
        values_c = np.nanmean(list(df_ss[(df_ss["Entry_Isoform"] == row["Entry_Isoform"]) & (df_ss["RES_NUM"] >= row["START"]) & (
                df_ss["RES_NUM"] <= row["END"])]["P(C)"].values))
        values_h = np.nanmean(list(df_ss[(df_ss["Entry_Isoform"] == row["Entry_Isoform"]) & (df_ss["RES_NUM"] >= row["START"]) & (
                df_ss["RES_NUM"] <= row["END"])]["P(H)"].values))
        values_e = np.nanmean(list(df_ss[(df_ss["Entry_Isoform"] == row["Entry_Isoform"]) & (df_ss["RES_NUM"] >= row["START"]) & (
                df_ss["RES_NUM"] <= row["END"])]["P(E)"].values))
        values_asa = np.nanmean(list(df_ss[
                                         (df_ss["Entry_Isoform"] == row["Entry_Isoform"]) & (
                                                 df_ss["RES_NUM"] >= row["START"]) & (
                                                 df_ss["RES_NUM"] <= row["END"])]["ASA"].values))
        return values_asa, values_c, values_h, values_e

    except:
        return np.nan, np.nan, np.nan, np.nan





def concat(grp):


    l = []
    for value in grp:
        l.append(str(value))
    return ",".join(l)

'''
Load the data
'''


def load_data(simulated=True,simulated_path=path_default_simulated):
    '''
    Loads the data for calculation of features
    :param simulated: Whether perform the z-score calculation
    :param simulated_path: Path of default simulated dataframe
    :return:
    '''
    if simulated:

        simulated_values = pd.read_csv(simulated_path,compression="gzip",sep="\t")  # Data generated in Calculate_Features_All_Degrons_SLIM_Features_importance(last_version).ipynb

    else:
        simulated_values = pd.DataFrame([],columns=["Entry", "Entry_Isoform", "DEGRON", "START", "END","ASA_SCORE","CONS_SCORE",
                                                     "FCONS_SCORE", "COIL", "HELIX","STRAND","DSS_SCORE", "RIG_SCORE", "ANCHOR_SCORE",
                                                     "Domain_pfam", "nflanking_ub_lysines","ub_lysines", "nflanking_lysines","any_lysines", "nflanking_ptms","ptms_flanking", "Z_ASA", "Z_DSS", "Z_CONS",
                                                     "Z_FCONS", "Z_SS_C", "Z_SS_H", "Z_SS_E", "Z_RIG", "Z_ANCHOR",
                                                     "Z_DOMAIN", "Z_FLANKING_UB_LYSINE", "Z_FLANKING_LYSINE", "Z_PTMS"])

    df_pfam = pd.read_csv(path_pfam, sep="\t", comment="#")
    df_pfam = df_pfam[df_pfam["TYPE"] == "Domain"][["Entry", "E_START", "E_END", "PFAM_ID"]].drop_duplicates()
    df_pfam_grouped = df_pfam.groupby("Entry", as_index=False).agg({"E_START": concat, "E_END": concat, "PFAM_ID": concat})
    return simulated_values, df_pfam_grouped


'''
Calculate basic properties

'''


def calculate_basic_properties(df):
    '''

    :param df: a dataframe with columns "Entry","Entry_Isoform","START","END","DEGRON"
    :return: includes in the dataset the columns for ASA,DSS,COILD,HELIX,STRAND,ANCHOR,FLEX,CONS,FLANKING_CONS
    '''
    list_scores = []
    for index, row in df.iterrows():


        # get dss
        try:
            dss = get_dss(row)
        except:
            dss = np.nan
        try:
            # get asa, c, h, e
            asa, c, h, e = get_asa_protein(row)
        except:
            asa, c, h, e = np.nan, np.nan, np.nan, np.nan 
            # get anchor
        try:
            anchor = get_anchor(row)
        except:
            anchor = np.nan
        try:
            # get flexibilty
            values_flex = get_flex_protein(row)
        except:
            values_flex = np.nan
        try:
            # get conservation
            cons = get_cons(row)
        except: 
            cons = np.nan
        try:
            # get conservation compared to flaking
            fcons = get_cons_flanking(row)
        except:
            fcons = np.nan

        list_scores.append(
            [row["Entry"],row["Entry_Isoform"], row["START"], row["END"], row["DEGRON"], asa, dss, c, h, e,
             anchor, values_flex, cons, fcons])
    score_degrons = pd.DataFrame(list_scores,
                                           columns=["Entry", "Entry_Isoform", "START", "END", "DEGRON", "ASA_SCORE", "DSS_SCORE", "COIL", "HELIX",
                                                    "STRAND", "ANCHOR_SCORE", "RIG_SCORE", "CONS_SCORE", "FCONS_SCORE"])
    return score_degrons


'''
Functions to include the pfam domain
'''


def find_hit_pfam(row):

    list_pfams = row["PFAM_ID"].split(",")
    if len(list_pfams) == 0 or row["PFAM_ID"]=="None":
        return "None"
    list_start = str(row["E_START"]).split(",")
    list_end = str(row["E_END"]).split(",")
    output = []
    degron_range = range(int(row["START"]),int(row["END"])+1)
    degron_s = set(degron_range)
    for i in range(0,len(list_pfams)):
        domain_range = range(int(list_start[i]),int(list_end[i])+1)
        if len(degron_s.intersection(domain_range)) > 0:
            output.append(list_pfams[i])
    if len(output) == 0:
        return "None"
    return ",".join(output)


def add_pfam(df,df_pfam):
    '''
    :param df: dataframe query
    :param df_pfam: dataframe of grouped pfam
    :return: df with the pfam information
    '''
    df = pd.merge(left=df_pfam[["Entry", "E_START", "E_END", "PFAM_ID"]].drop_duplicates(),
                                       right=df, left_on=["Entry"], right_on=["Entry"],
                                       how="right")
    df["PFAM_ID"].fillna("None", inplace=True)
    df["hit_pfam"] = df.apply(lambda row: find_hit_pfam(row), axis=1)
    df["Domain_pfam"] = df.apply(lambda row: 0 if row["hit_pfam"] == "None" else 1,
                                                                      axis=1)
    df.drop(["E_START", "E_END", "PFAM_ID"], axis=1, inplace=True)
    return df


'''
Get the means of the simulated values 
'''


def get_mean(simulated_values):

    mean_asa = np.nanmean(simulated_values["ASA_SCORE"].values)
    std_asa = np.nanstd(simulated_values["ASA_SCORE"].values)
    mean_dss = np.nanmean(simulated_values["DSS_SCORE"].values)
    std_dss = np.nanstd(simulated_values["DSS_SCORE"].values)
    mean_ss_c = np.nanmean(simulated_values["COIL"].values)
    std_ss_c = np.nanstd(simulated_values["COIL"].values)
    mean_ss_h = np.nanmean(simulated_values["HELIX"].values)
    std_ss_h = np.nanstd(simulated_values["HELIX"].values)
    mean_ss_e = np.nanmean(simulated_values["STRAND"].values)
    std_ss_e = np.nanstd(simulated_values["STRAND"].values)
    mean_cons = np.nanmean(simulated_values[simulated_values["CONS_SCORE"] >= 0.0]["CONS_SCORE"].values)
    std_cons = np.nanstd(simulated_values[simulated_values["CONS_SCORE"] >= 0.0]["CONS_SCORE"].values)
    mean_fcons = np.nanmean(simulated_values[simulated_values["FCONS_SCORE"] >= 0.0]["FCONS_SCORE"].values)
    std_fcons = np.nanstd(simulated_values[simulated_values["FCONS_SCORE"] >= 0.0]["FCONS_SCORE"].values)
    mean_flex = np.nanmean(simulated_values["RIG_SCORE"].values)
    std_flex = np.nanstd(simulated_values["RIG_SCORE"].values)
    mean_anchor = np.nanmean(simulated_values["ANCHOR_SCORE"].values)
    std_anchor = np.nanstd(simulated_values["ANCHOR_SCORE"].values)
    mean_domain = np.nanmean(simulated_values["Domain_pfam"].values)
    std_domain = np.nanstd(simulated_values["Domain_pfam"].values)
    mean_ub_lysines = np.nanmean(simulated_values["nflanking_ub_lysines"].values)
    std_ub_lysines = np.nanstd(simulated_values["nflanking_ub_lysines"].values)
    mean_flanking_lysines = np.nanmean(simulated_values["nflanking_lysines"].values)
    std_flanking_lysines = np.nanstd(simulated_values["nflanking_lysines"].values)
    mean_ptms_nearby = np.nanmean(simulated_values["nflanking_ptms"].values)
    std_ptms_nearby = np.nanstd(simulated_values["nflanking_ptms"].values)
    return (mean_asa,std_asa,mean_dss,std_dss,mean_ss_c,std_ss_c,mean_ss_h,std_ss_h,mean_ss_e,std_ss_e,mean_cons,std_cons,mean_fcons,std_fcons,mean_flex,std_flex,mean_anchor,std_anchor,mean_domain,std_domain,mean_ub_lysines,std_ub_lysines,mean_flanking_lysines,std_flanking_lysines,mean_ptms_nearby,std_ptms_nearby)


'''
Global function
'''


def calculate_biochemical_properties(df, simulations=True, simulated_path=path_default_simulated, lysines=(5,10), ptms=10):

    '''

    :param df:
    :return:
    '''

    simulated_values,df_pfam = load_data(simulated_path)
    if simulations:
        simulated_values = simulated_values.dropna()
        simulated_values = simulated_values[(simulated_values["CONS_SCORE"]>0.0)] # Get only simulations with defined score
        simulated_values = simulated_values.dropna(axis=0)
    dg = calculate_basic_properties(df)
    dh = add_pfam(dg,df_pfam)
    di = find_sorrounding_lysines(dh,limit=lysines[1],init=lysines[0])
    dj = find_sorrounding_phospho(di,limit=ptms)
    (mean_asa, std_asa, mean_dss, std_dss, mean_ss_c, std_ss_c, mean_ss_h, std_ss_h, mean_ss_e, std_ss_e, mean_cons,
     std_cons, mean_fcons, std_fcons, mean_flex, std_flex, mean_anchor, std_anchor, mean_domain, std_domain,
     mean_ub_lysines, std_ub_lysines, mean_flanking_lysines, std_flanking_lysines, mean_ptms_nearby, std_ptms_nearby) = get_mean(simulated_values)
    list_values = []

    for index, row in dj.iterrows():

        zscore_asa = (row["ASA_SCORE"] - mean_asa) / std_asa
        zscore_cons = (row["CONS_SCORE"] - mean_cons) / std_cons
        zscore_fcons = (row["FCONS_SCORE"] - mean_fcons) / std_fcons
        zscore_ss_coil = (row["COIL"] - mean_ss_c) / std_ss_c
        zscore_ss_helix = (row["HELIX"] - mean_ss_h) / std_ss_h
        zscore_ss_strand = (row["STRAND"] - mean_ss_e) / std_ss_e
        zscore_dss = (row["DSS_SCORE"] - mean_dss) / std_dss
        zscore_flex = (row["RIG_SCORE"] - mean_flex) / std_flex
        zscore_anchor = (row["ANCHOR_SCORE"] - mean_anchor) / std_anchor
        zscore_domain = (row["Domain_pfam"] - mean_domain) / std_domain
        zscore_ub_lysines = (row["nflanking_ub_lysines"] - mean_ub_lysines) / std_ub_lysines
        zscore_f_lysines = (row["nflanking_lysines"] - mean_flanking_lysines) / std_flanking_lysines
        zscore_ptms = (row["nflanking_ptms"] - mean_ptms_nearby) / std_ptms_nearby
        list_values.append(
            [row["Entry"], row["Entry_Isoform"], row["DEGRON"], row["START"], row["END"],row["ASA_SCORE"],row["CONS_SCORE"],row["FCONS_SCORE"],row["COIL"],row["HELIX"],row["STRAND"],row["DSS_SCORE"]
            ,row["RIG_SCORE"],row["ANCHOR_SCORE"],row["Domain_pfam"],row["nflanking_ub_lysines"],row["ub_lysines"],row["nflanking_lysines"],row["any_lysines"],row["nflanking_ptms"],row["ptms_flanking"],zscore_asa, zscore_dss, zscore_cons, zscore_fcons,
             zscore_ss_coil, zscore_ss_helix, zscore_ss_strand, zscore_flex, zscore_anchor, zscore_domain,
             zscore_ub_lysines, zscore_f_lysines, zscore_ptms])

    zscore_degrons = pd.DataFrame(list_values,
                                            columns=["Entry", "Entry_Isoform", "DEGRON", "START", "END","ASA_SCORE","CONS_SCORE",
                                                     "FCONS_SCORE", "COIL", "HELIX","STRAND","DSS_SCORE", "RIG_SCORE", "ANCHOR_SCORE",
                                                     "Domain_pfam", "nflanking_ub_lysines","ub_lysines", "nflanking_lysines","any_lysines", "nflanking_ptms","ptms_flanking", "Z_ASA", "Z_DSS", "Z_CONS",
                                                     "Z_FCONS", "Z_SS_C", "Z_SS_H", "Z_SS_E", "Z_RIG", "Z_ANCHOR",
                                                     "Z_DOMAIN", "Z_FLANKING_UB_LYSINE", "Z_FLANKING_LYSINE", "Z_PTMS"])
    zscore_degrons.fillna(0.0, inplace=True)
    df_final = pd.merge(zscore_degrons,df,how="right")
    return df_final



