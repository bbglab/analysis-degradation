#!/usr/bin/env python
# encoding: utf-8
"""
predictor.py

Created by Elisa Cilia on 2014-08-20.
Copyright (c) 2014 Elisa Cilia. All rights reserved.
"""

import sys
import os
import urllib2
import zipfile
from Bio import SeqIO
import re

import config
from submit import DynaMineJSONInterface


class DynaMine:
    def __init__(self, jobdir, light=False, allinoneout=False):
        self._light = light
        self._allinone = allinoneout
        self._jobdir = jobdir

    @staticmethod
    def version():
        version = '' 
        try:
            data = urllib2.urlopen(os.path.join(config.configuration_file['server_name'], 'version'))
            version = 'DynaMine v ' + data.read().strip()
        except Exception, e:
            version = 'ERROR while communicating with the DynaMine server.'
        return version

    def predict(self, filename):
        seqs = self._getinputsequences(filename)
        ji = DynaMineJSONInterface(config.configuration_file['json_api_key'])
        results = ji.submit_sequences(seqs, predictions_only = self._light)
        # print(results)
        if 'status' in results and results['status'] == 'error':
            print results['message']
            return False            
        else:
            self._save(results)
            return True

    def _save(self, results):
        filename = results['url'].split('/')[-1]
        path = os.path.join(self._jobdir, filename)
        try:
            data = urllib2.urlopen(results['url'])
            fd = open(path, 'w')
            fd.write(data.read())
            fd.close()
        except urllib2.HTTPError:
            sys.stderr.write("\nData %s not found on the server.\n" % (filename))
        except Exception, e:
            sys.stderr.write(str(e))
        with zipfile.ZipFile(path) as zf:
            zf.extractall(self._jobdir)
        os.remove(path)

    def _getinputsequences(self, filename):
        seqs = {}
        sequences = SeqIO.parse(filename, "fasta")
        for record in sequences:
            s = re.sub('[^0-9a-zA-Z-]+', '_', record.id.lstrip('>'))
            seqs[s] = str(record.seq)
        return seqs

