#!/usr/bin/env python
# encoding: utf-8
"""
dynamine.py

Created by Elisa Cilia on 2014-08-20.
Copyright (c) 2014 Elisa Cilia. All rights reserved.
"""

import sys
import os
import argparse

from predictor.predictor import DynaMine


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='DynaMine predictions.')
    parser.add_argument('infile', help='input file containing sequence/s in FASTA format.')
    parser.add_argument('-l', '--light-implementation', action="store_true", help='run a lighter DynaMine version.')
    parser.add_argument('-n', '--onefile', action="store_true", help='write the output predictions of all input sequences in a single file.')
    parser.add_argument('-a','--add-to-directory', action='store_true', help='add information to existing directory')
    parser.add_argument('-o','--output_path',help='output path of the file')
    parser.add_argument('--version', action='version', version=DynaMine.version())
    args = parser.parse_args()
    
    fbname =  args.output_path
    
    if os.path.exists(fbname):
        if not args.add_to_directory:
            print "Error: the directory %s exists! Use option -add to overwrite it." % (fbname)
            sys.exit(1)
    else:
        os.mkdir(fbname)

    dynamine = DynaMine(fbname, light=args.light_implementation, allinoneout=args.onefile)
    if dynamine.predict(args.infile):
        print 'The results are in the \'%s\' folder.' % (fbname)
