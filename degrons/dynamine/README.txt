To use the DynaMine predictor:

1. Get an API key from http://dynamine.ibsquare.be/download

2. Edit the config.txt file by adding the API key that you received by email as shown below:
    json_api_key = 'your_key_here'

3. Launch:
    pip install biopython

4. Run the DynaMine predictor by launching:
    dynamine.py <your input file in FASTA format>
