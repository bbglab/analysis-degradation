import requests
import sys
import click
import os

@click.group()
def cli():
    pass

@cli.command(context_settings={'help_option_names': ['-h', '--help'], 'max_content_width': 200})
@click.option('--uniprot', help='uniprot identifier')
@click.option('--output_path', help='path to store the MSA')
def generatemsa(uniprot, output_path):
    file_out = output_path+uniprot+".fa"
    if os.path.exists(file_out):
        print ("File ",file_out," exists ")
        return None
    if "-" in uniprot:
        uniprot = uniprot.split("-")[0]
    url = "http://bioware.ucd.ie/rest/gopher?uniprotid="+uniprot+"&taxonid=9606"
    resp = requests.get(url)
    f = open(file_out,'wb')
    f.write(resp.content)
    f.close()
    # Now replace the first line with the uniprot ID, so then we can easily find the target of the MSA
    with open(file_out) as f:
        lines = f.readlines()

    lines[0] = ">"+uniprot+"\n"

    with open(file_out, "w") as f:
        f.writelines(lines)
    f.close()
