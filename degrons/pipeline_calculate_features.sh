#!/usr/bin/env bash

# Pipeline to calculate features
#/workspace/users/fmartinez/scripts_biochemical_features <--- Path to the scripts

set -e

# Pipeline to calculate features of degrons
# ---------------------------

# Help prompt

if [ "$1" == "--help" ]; then
	echo "Usage: bash calculate_features `basename $0` uniprot-isoform"
	echo ""
	echo "Pipeline to calculate biochemical properties of proteins. Properties are: "
	echo ""
	echo "- Flanking conservation score: two steps are necessary here:"
	echo "i) First, for each input sequence multiple sequence alignment of orthologs proteins were calculated using the Gopher tool from Bioware (http://bioware.ucd.ie/~compass/biowareweb/Server_pages/gopher.php)."
	echo "ii)  Next, residue specific conservation score was calculated using Capra JA. and Singh M. method (scoring=shannong_entropy; gap_cutoff=0.9; matrix=blosum62). "
	echo ""
	echo "- Solvent Accesibility and Secondary Structure: Calculated by SPIDER2 10.1007/978-1-4939-6406-2_6"
	echo ""
	echo "- Stabilization upon binding: Calculated by ANCHOR doi:10.1371/journal.pcbi.1000376"
	echo ""
	echo "- Rigidity: Calculated by Dynamine 10.1038/ncomms3741"
	echo ""
	echo "- Disorder: Calculated by IUPRED 10.1016/j.jmb.2005.01.071"
	echo ""
     # Help prompt

	exit 0
fi

base=/workspace/projects/ubiquitins/code/ #<--- Customize with your path
uniprot_isoform=$1
IFS='-' read -ra NAMES <<< "$uniprot_isoform"
uniprot=${NAMES[0]}
path_scripts=${base}/degrons/



# -- Calculating conservation score
echo "Step 1. Calculating conservation score---"
source activate calculate_features_python2

path_script_MSA=${path_scripts}cons_score/get_MSA.py

path_fasta=${base}/data/sequences/SEQUENCES_ISOFORMS
path_MSA=${base}/data/sequences/MSA/
path_matrix_blosum62=${base}/degrons/data/blosum62.bla
msa_file=${path_MSA}/${uniprot}.fa

echo ""

echo "Step 1a. Generate MSA of input uniprot"
echo ""

if [ ! -f ${msa_file} ]; then
    echo "python2.7 ${path_script_MSA} generatemsa --uniprot ${uniprot} --output_path ${path_MSA}"
    python2.7 ${path_script_MSA} generatemsa --uniprot ${uniprot} --output_path ${path_MSA}
else

    echo "File ${msa_file} already exists! Skipping step."
fi

echo ""

echo "Step 1b. Generate the conservation score based on MSA"
echo ""

path_script_cons=${path_scripts}cons_score/score_conservation.py
path_score_cons=${path_MSA}/Scores

if [ ! -f ${path_score_cons}/${uniprot}.score ]; then

    echo "python2.7 ${path_script_cons} -a ${uniprot} -s shannon_entropy \
    -g 0.9 -m ${path_matrix_blosum62} \
    ${msa_file} > ${path_score_cons}/${uniprot}.score"\

    python2.7 ${path_script_cons} -a ${uniprot} -s shannon_entropy \
    -g 0.9 -m ${path_matrix_blosum62} \
    ${msa_file} > ${path_score_cons}/${uniprot}.score

else
    echo "File ${path_score_cons}/${uniprot}.score already exists! Skipping step"

fi

echo ""

echo "Step 2. Run ANCHOR software to calculate stabilization upon binding"

echo ""

#  Calculate  ANCHOR
path_script_anchor=${path_scripts}anchor/anchor.pl
path_output_anchor=${base}/data/features/ANCHOR

if [ ! -f ${path_output_anchor}/${uniprot_isoform}.fa.out  ]; then
    echo "perl  ${path_script_anchor} ${path_fasta}/${uniprot_isoform}.fa > ${path_output_anchor}/${uniprot_isoform}.fa.out"
    perl  ${path_script_anchor} ${path_fasta}/${uniprot_isoform}.fa > ${path_output_anchor}/${uniprot_isoform}.fa.out
    echo "Output stored in ${path_output_anchor}/${uniprot_isoform}.fa.out"

else
    echo "File ${path_output_anchor}/${uniprot_isoform}.fa.out already exists! Skpping step"

fi


echo ""

# Calculate Solvent Accesibility and Secondary Structure

path_script_spider=${path_scripts}spider/pred_pssm.py
path_uniref_50=/workspace/users/fmartinez/SPIDER/uniref50/nr.50 # Path to uniref50
path_output_ss=${base}/data/features/ASA



echo "Step 3. Run SPIDER2 to calculate Solvent Accesiblity and Secondary Structure."

echo ""

beforedot=${arg%.*}


if [ ! -f ${path_output_ss}/${uniprot_isoform}.spd3  ]; then
    echo "psiblast -db ${path_uniref_50} -num_iterations 3 -num_alignments 1 -num_threads 4 -query ${path_fasta}/${uniprot_isoform}.fa \
    -out_ascii_pssm ${path_output_ss}/${uniprot_isoform}.pssm"
    psiblast -db ${path_uniref_50} -num_iterations 3 -num_alignments 1 -num_threads 4 -query ${path_fasta}/${uniprot_isoform}.fa \
    -out_ascii_pssm ${path_output_ss}/${uniprot_isoform}.pssm 2>  /dev/null > /dev/null
    echo ""
    echo "python2.7 ${path_script_spider}  ${path_output_ss}/${uniprot_isoform}.pssm ${path_output_ss}/${uniprot_isoform}.spd3"
    python2.7 ${path_script_spider}  ${path_output_ss}/${uniprot_isoform}.pssm ${path_output_ss}/${uniprot_isoform}.spd3
else

    echo "File ${path_output_ss}/${uniprot_isoform}.spd3 already exists! Skipping step"

fi



echo ""


echo "Step 4. Run Dynamine to calculate amino-acid specific rigidity score"

echo ""

path_script_dynamine=${path_scripts}dynamine/dynamine.py
path_output_dynamine=${base}/data/features/FLEX/

if [ ! -f ${path_output_dynamine}/${uniprot_isoform}_backbone.pred  ]; then
    echo "python ${path_script_dynamine}  ${path_fasta}/${uniprot_isoform}.fa -a -o ${path_output_dynamine}"
    python ${path_script_dynamine}  ${path_fasta}/${uniprot_isoform}.fa -a -o ${path_output_dynamine}
    echo ""
    echo "mv ${path_output_dynamine}Dynamine*/${uniprot_isoform}_backbone.pred ${path_output_dynamine}" # Move the output from the directory to the output path
    mv ${path_output_dynamine}Dynamine*/${uniprot_isoform}_backbone.pred ${path_output_dynamine} # Move the output from the directory to the output path
    echo ""
    echo "rm -rf ${path_output_dynamine}Dynamine*/" # Remove the tmp directory
    rm -rf ${path_output_dynamine}Dynamine*/ # Remove the tmp directory
else

    echo "File ${path_output_dynamine}/${uniprot_isoform}._backbone.pred already exists! Skipping step"

fi
echo ""


echo "Step 5. Run IUPRED to calculate disorder"

echo ""

path_script_iupred=${path_scripts}iupred/iupred
path_output_iupred=${base}/data/features/DSS/


if [ ! -f ${path_output_iupred}/${uniprot_isoform}.fa  ]; then
    set IUPred_PATH=${path_script_iupred}
    echo "${path_script_iupred}  ${path_fasta}/${uniprot_isoform}.fa short > ${path_output_iupred}/${uniprot_isoform}.fa"
    ${path_script_iupred}  ${path_fasta}/${uniprot_isoform}.fa short > ${path_output_iupred}/${uniprot_isoform}.fa
    echo ""
    echo "perl -p -i -e 's/^\s+//g' ${path_output_iupred}/${uniprot_isoform}.fa" # Remove the white spaces at beg of line
    perl -p -i -e 's/^\s+//g' ${path_output_iupred}/${uniprot_isoform}.fa
    echo "perl -p -i -e 's/\h+/\t/g' ${path_output_iupred}/${uniprot_isoform}.fa" # Replace multiple white spaces for a tab
    perl -p -i -e 's/\h+/\t/g' ${path_output_iupred}/${uniprot_isoform}.fa

else

    echo "File ${path_output_iupred}/${uniprot_isoform}.fa already exists! Skipping step"

fi
echo ""

echo "Done..."

exit 0


