import pandas as pd
import numpy as np
import os


path_base = "../"
path_phosphosites = os.path.join(path_base,"data","phosphorylation_sites_human.tsv.gz")



def get_position(row):
    '''

    :param row:
    :return:
    '''
    for mut in row["Protein_Mutations"].split(","):
        if len(mut)>1:
            return mut[1:-1]
def concat(grp):

        l = []
        for value in grp:
            l.append(str(value))
        return ",".join(l)

def find_hit(row,limit,exclude,init=1):

        list_positions = str(row["Position"]).split(",")
        list_residues = str(row["MOD_RSD"]).split(",")
        if str(row["Position"]) == "nan":
            return np.nan
        output = []
        if exclude: # Not include degron
            degron_range = range(int(row["START"])-limit, int(row["START"]) -init+1)
            degron_range = list(degron_range) + list(range(int(row["END"])+init, int(row["END"]) + limit+1))
        else:
            degron_range = range(int(row["START"])-limit,int(row["END"]+limit+1))
        degron_s = set(degron_range)
        for i in range(0, len(list_positions)):
            domain_range = range(int(list_positions[i]), int(list_positions[i]) + 1)
            if len(degron_s.intersection(domain_range)) > 0:
                output.append(list_residues[i])
        if len(output) == 0:
            return np.nan
        return ",".join(output)




def find_sorrounding_phospho(df,limit=11,exclude=False):
    '''
    Function to calculate sorrounding phosphorilation sites
    :param df: the input dataframe
    :param limit: end of the search (default 11 amino acids)
    :param exclude: Whether the degron should be included or not, default included
    :return:
    '''

    try:
        if "MOD_RSD" in df.columns.values:
            df.drop(["MOD_RSD"], axis=1, inplace=True)
        if "Position" in df.columns.values:
            df.drop(["Position"], axis=1, inplace=True)
        if "ptms_flanking" in df.columns.values:
            df.drop(["ub_lysines"], axis=1, inplace=True)
        if "nflanking_ptms" in df.columns.values:
            df.drop(["nflanking_ub_lysines"], axis=1, inplace=True)
              
    except ValueError:
        print ("Error in creation of the columns")

    df_ptm_sites = pd.read_csv(path_phosphosites,sep="\t",compression="gzip")
    df_ptm_sites.rename(columns={"ACC_ID":"Entry"},inplace=True)
    df_ptm_sites = df_ptm_sites.groupby("Entry",as_index=False).agg({"MOD_RSD":concat,"Position":concat})
    df = pd.merge(left=df,right=df_ptm_sites[["MOD_RSD", "Entry", "Position"]].drop_duplicates(),how="left")
    df["ptms_flanking"] = df.apply(lambda row: find_hit(row,limit,exclude),axis=1)
    df["nflanking_ptms"] =  df.apply(lambda row: 0 if str(row["ptms_flanking"])=="nan" else len(str(row["ptms_flanking"]).split(",")),axis=1)
    return df





