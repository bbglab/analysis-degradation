import json
import pandas as pd
import numpy as np
import re
import os





class Score_Calculator():
    # DEFINITION OF GLOBAL VARIABLES
    MAX_DIST = 12.0 # MAXIMUM DISTANCE
    MIN_DIST_SCORE = 3.0 # BASE SCORE OF THE FLANKING POSITIONS
    FLANKING_POS = 11 # NUMBER OF FLANKING POSITIONS
    
    # DEFINITION OF POSIBLE CONSEQUENCES OF MISSENSE MUTATIONS
    list_classes = ['Mutation_Altering_Motif','Mutation_Flanking_PTM', 'Mutation_UB_Flanking_Lysine','Mutation_Flanking_Lysine','Mutation_Flanking_Degron']

    def __init__(self):
        self.dict_base = {"Mutation_Altering_Motif": 5.0, "Mutation_UB_Flanking_Lysine": 4.0,
                          "Mutation_Flanking_PTM": 4.0,
                          "Mutation_Flanking_Lysine": 1.0, "Mutation_Flanking_Motif": 0.0}

        df_ubi_sites,df_ptm_sites,df_sequences,df_total_degrons,df_patterns,d_query = Score_Calculator.load_data()
        self.sequences = df_sequences
        self.df_degrons = df_total_degrons[~df_total_degrons["DEGRON"].str.contains("USP",na=False)]
        self.patterns = df_patterns
        self.dict_query = d_query
        self.df_ptm = df_ptm_sites

        self.df_ubi = df_ubi_sites
        self.normalized_scale_flex = Score_Calculator.get_normalized_scales()[0]
        self.normalized_scale_hb = Score_Calculator.get_normalized_scales()[1]
        self.min_score = 0.0
        self.max_score = self.dict_base["Mutation_Altering_Motif"]+1+1+1+1 # base + ptm + max_score flex + max_score asa + altered motif

    @staticmethod
    def load_data():
        base = "/workspace/projects/ubiquitins/codereview/degradation/data/"
        df_ubi_sites = pd.read_csv(os.path.join(base,"ubiquitination_sites_human.tsv.gz"),sep="\t",compression="gzip")
        df_ptm_sites = pd.read_csv(os.path.join(base, "phosphorylation_sites_human.tsv.gz"), sep="\t",compression="gzip")
        df_sequences = pd.read_csv(os.path.join(base,"sequences_isoforms.tsv"),sep="\t")
        df_total_degrons =  pd.read_csv(os.path.join(base,"predicte_degrons_properties.tsv.gz"),sep="\t",compression="gzip")
        df_patterns = pd.read_csv(os.path.join(base,"degron_classes.tsv"),sep="\t")
        d_query = json.load(
            open(os.path.join(base,"aas_degrons_isoforms.json"), "r"))
        return (df_ubi_sites, df_ptm_sites, df_sequences, df_total_degrons, df_patterns, d_query)

    @staticmethod
    def get_normalized_scales():
        '''
        Method to return the normalize scales
        :return: scale_flex,normalize_scale_hb
        '''
        scale_flex = {"H": 0.32, "R": 0.53, "K": 0.47, "D": 0.51, "E": 0.5, "A": 0.36, "L": 0.37, "I": 0.46, "P": 0.51,
                      "W": 0.31, "C": 0.35, "G": 0.54, "T": 0.44, "S": 0.51, "N": 0.46, "F": 0.31, "Y": 0.42, "Q": 0.49,
                      "M": 0.3, "V": 0.39}
        normalize_scale_flex = {}
        minv = np.nanmin(list(scale_flex.values()))
        maxv = np.nanmax(list(scale_flex.values()))
        for x in scale_flex.keys():
            normalize_scale_flex[x] = (scale_flex[x] - minv) / (maxv - minv)

        # http://web.expasy.org/protscale/pscale/Hphob.Eisenberg.html J. Mol. Biol. 179:125-142(1984).
        scale_hb = {"H": -0.40, "R": -2.53, "K": -1.5, "D": -0.90, "E": -0.740, "A": 0.620, "L": 1.06, "I": 1.38,
                    "P": 0.12, "W": 0.81, "C": 0.29, "G": 0.48, "T": 0.26, "S": -0.18, "N": -0.78, "F": 1.19,
                    "Y": -0.05, "Q": -0.85, "M": 0.640, "V": 1.08}

        normalize_scale_hb = {}
        minv = np.nanmin(list(scale_hb.values()))
        maxv = np.nanmax(list(scale_hb.values()))
        for x in scale_hb.keys():
            normalize_scale_hb[x] = (scale_hb[x] - minv) / (maxv - minv)


        return normalize_scale_flex,normalize_scale_hb


    def is_in_degron(self,mutation,entry):
        '''

        :param hugo: protein Hugo_Symbol
        :param entry: protein Entry (in case of Hugo_Symbol=="")
        :param mutation: mutation to query
        :return: whether the mutation falls in a DEGRON area
        '''
        try:
            pos = int(mutation[1:len(mutation)-1])

            if not(entry in self.dict_query):
                return False
            if pos in self.dict_query[entry]:
                return True
        except:
            return False
        return False


    def is_ubiquitinated(self,entry,pos):
        '''

        :param entry: Uniprot ID of the query protein
        :param pos: Integer position of the mutation
        :return: Boolean, the amino acid is ubiquitinated
        '''
        query = "K"+str(pos)+"-ub"
        if self.df_ubi[(self.df_ubi["ACC_ID"]==entry)&(self.df_ubi["MOD_RSD"]==query)].shape[0] > 0:
            return True
        return False
    def is_ptm(self,entry,mutation):
        '''

        :param entry: Uniprot ID of the query protein
        :param pos: Integer position of the mutation
        :return:  Boolean, the amino acid has a PTM annotated
        '''
        query = mutation[0:len(mutation)-1]
        entry = entry.split("-")[0]
        if self.df_ptm[(self.df_ptm["ACC_ID"]==entry)&(self.df_ptm["MOD_RSD"].str.contains(query))].shape[0] > 0:
            return True
        return False

    def is_lysine(self,mutation):
        '''

        :param mutation: Query mutation
        :return: Boolean, whether the mutation is in a lysine
        '''
        return mutation[0] == "K"

    def get_degrons(self,entry,pos,mutation,limit=11):
        '''
        Select the roles of this mutation
        :param entry: UP id
        :param pos: position of the mutation
        :param mutation: the mutation
        :param limit: flanking limit
        :return: list of phenotypes of this mutation
        '''
        list_degrons = [] # list of tuples DEGRON,START,END,CLASS
        for index,row in self.df_degrons[self.df_degrons["Entry_Isoform"]==entry].iterrows():
            # Check whether is involved in the degron
            if row["START"] <= pos and row["END"]>= pos:
                # Falls into the motif
                classp = "Mutation_Altering_Motif"

            elif (row["START"]-limit <= pos and row["START"] > pos) or (row["END"]+limit >= pos and row["END"] < pos) : # Flanking region

                if self.is_ptm(entry,mutation):
                    classp = "Mutation_Flanking_PTM"
                elif self.is_ubiquitinated(entry.split("-")[0],pos):
                    classp = "Mutation_UB_Flanking_Lysine"
                elif self.is_lysine(mutation):
                    classp = "Mutation_Flanking_Lysine"
                else:
                    classp = "Mutation_Flanking_Motif"
            else:
                continue # Not inolved in this degron
            list_degrons.append((row["DEGRON"],row["START"],row["END"],classp,row["Prob_DEGRON"]))

        return list_degrons

    def get_classes(self,mutation,entry):
        '''
        :param mutation: given a mutation involved in a degron
        :return: returns the dominant clas of the mutation for each of the degrons that it is involved
        '''
        try:
            pos = int(mutation[1:len(mutation)-1])
        except:
            return []
        list_classes = self.get_degrons(entry,pos,mutation)
        return list_classes


    def calculate_change_flexibility(self,mutation):
        '''
        Given a mutation returnts the change in Flexiblity upon mutation
        :return:
        '''

        wt_aa = mutation[0]
        mt_aa = mutation[len(mutation)-1]
        try:
            return np.abs(self.normalized_scale_flex[wt_aa]-self.normalized_scale_flex[mt_aa])
        except:
            return -2.0



    def is_altered_regex(self,mutation,entry,degron,start,end):
        '''

        :param mutation: Mutation to be queried
        :param entry: Uniprot ID
        :param degron: Name of the degron to be queried
        :return: "YES" if the mutation alters the pattern, "NO" if it does not, "UNKNOWN" if the sequences does not match
        '''
        mt_seq =  self.get_mutated_seq(mutation,entry)
        if len(mt_seq) == 0:

            return "UNKNOWN"
        mt_motif = mt_seq[int(start)-1:int(end)]

        return self.is_altered(mt_motif,degron)



    def get_mutated_seq(self,mutation,entry):
        '''
        :param mutation
        :param entry
        :return the mutated sequence or empty list
        '''

        seq = self.sequences[self.sequences["Entry_Isoform"]==entry]["Sequence"].values
        if len(seq) == 0:

            return ""
        else:
            seq_wt = "".join(seq)
            try:
                pos = int(mutation[1:len(mutation)-1])
                if seq_wt[pos-1] == mutation[0]: # Sequence matches
                    seq_wt = list(seq_wt)

                    seq_wt[pos-1] = mutation[len(mutation)-1]
                    return "".join(seq_wt)
                else:
                    return ""
            except:

                return ""



    def is_altered(self,mt_seq,degron):
        '''

        :param mt_seq: Mutated sequence of the DEGRON only.
        :param degron: Name of the degron
        :return:
        '''

        Regex = self.patterns[(self.patterns["ELMIdentifier"]==degron)]["Regex"].values
        if (len(Regex) == 0):
            return False
        p = re.compile(Regex[0])

        if(p.match(mt_seq.upper()) is None):
            return True
        else:
            return False

    def calculate_change_hydrophobicity(self,mutation):
        # http://web.expasy.org/protscale/pscale/Hphob.Eisenberg.html J. Mol. Biol. 179:125-142(1984).
        scale_hb = {"H": -0.40, "R": -2.53, "K": -1.5, "D": -0.90, "E": -0.740, "A": 0.620, "L": 1.06, "I": 1.38,
                    "P": 0.12, "W": 0.81, "C": 0.29, "G": 0.48, "T": 0.26, "S": -0.18, "N": -0.78, "F": 1.19,
                    "Y": -0.05, "Q": -0.85, "M": 0.640, "V": 1.08}
        wt_aa = mutation[0]
        mt_aa = mutation[len(mutation) - 1]
        try:
            return np.abs(self.normalized_scale_hb[wt_aa] - self.normalized_scale_hb[mt_aa])
        except:
            return np.nan

    def get_normalized_score(self,value):
        '''
        Normalizes from 0 to 1 the output score
        :param value:
        :return:
        '''
        return (value - self.min_score) / (self.max_score - self.min_score)


    def get_score(self,degron,entry,start,end,classp,mutation):
        '''
        Get score of a mutation
        :param degron: The name of the degron
        :param entry: Uniprot ID of the query sequence
        :param start: STart position of the degron
        :param end: End position of the DEGRON
        :param classp: class label of the mutation
        :param mutation: mutation
        :return: The score of the mutation for a particular degron
        '''
        try:
            base = self.dict_base[classp]
        except:
            return 0.0
        if "Mutation_Altering" in classp:
            # Its an alteration of the DEGRON
            # Look whether its also an alteration in phosphodegron

            pptm = 0.0
            if self.is_ptm(entry,mutation):

                pptm=1.0

            # Change in flex
            change_flex = self.calculate_change_flexibility(mutation)
            change_asa = self.calculate_change_hydrophobicity(mutation)
            #Is there a change in the pattern?
            paltered = 0.0
            if  self.is_altered_regex(mutation,entry,degron,start,end):

                paltered = 1.0

            return (self.get_normalized_score(base + change_flex + change_asa  + paltered + pptm), pptm>0, paltered > 0)
        else:

            return (self.get_normalized_score(base + self.get_distance(mutation, start, end)),"PTM" in classp or "UB" in classp, False)

    def get_distance(self,mutation,start,end):
        '''

        :param mutation: Query mutation
        :param start: Start of the degron site
        :param end: End of the degron site
        :return:
        '''

        try:
            pos_mutation = int(mutation[1:len(mutation)-1])
        except ValueError:
            return 0.0

        if pos_mutation > end: # greater
            diff = pos_mutation - end
        else:
            diff = start - pos_mutation
        
        return (Score_Calculator.MIN_DIST_SCORE - (Score_Calculator.MIN_DIST_SCORE*(float(diff) / (Score_Calculator.MAX_DIST))))




    def get_scores(self,mutation,entry):
        '''

        :param mutation:
        :param entry:
        :return:
        '''
        if not(self.is_in_degron(mutation,entry)) or mutation[0] == mutation[len(mutation) - 1] or "*" in mutation:
            return [["-",0,0,mutation,"Non-Missense",entry,False,False,0.0]]
        list_scores = []
        list_classes = self.get_classes(mutation,entry)


        for mclass in list_classes:

            degron,start,end,classp,prob_degron = mclass
            score,ptm,altered =self. get_score(degron,entry,start,end,classp,mutation)
            list_scores.append([degron,start,end,mutation,classp,entry,ptm,altered,score])
        score = sorted(list_scores,key=lambda x: x[0],reverse=True)
        return [score[0]]




    def get_score_list_mutations_aslist(self, list_mutations):
        '''

        :param list_mutations: List of Tuples (Entry,Mutation) for calculating the score of each of the mutations
        :return: a dataframe with the scores of each mutation for each degron
        '''
        list_results = []
        for entry, mutation in list_mutations:
            # print (entry,mutation)
            list_scores = self.get_scores(mutation, entry)
            for score in list_scores:
                list_results.append(score)

        return list_results
