'''
Script that generates the jobs to run all uniprots of split files

'''
import glob
import os
output = "/workspace/projects/ubiquitins/scores/"
base_data_splits = "/workspace/projects/ubiquitins/annotation_transvar/"
base_data_source = "/workspace/projects/ubiquitins/codereview/degradation/data/"
base_script = "/workspace/projects/ubiquitins/codereview/degradation/create_dataframes/positive_selection/degrons/code/"
list_run = []
f_output = open(os.path.join(output,"commands_scores.sh"),'w')
f_output.write("[pre]\n")
f_output.write('. "/home/$USER/miniconda3/etc/profile.d/conda.sh"\n')
f_output.write("conda activate regression\n")
f_output.write("[params]\n")
cores = "1"
memory = "8G"
# parameters for all the jobs
f_output.write(f"cores = {cores}\n")
f_output.write(f"memory = {memory}\n")
f_output.write("[jobs]\n")
print ("Reading data from: "+base_data_splits+"split_uniprots_isoforms/"+"list_isoforms_*")
for file in glob.glob(base_data_splits+"split_uniprots_isoforms/"+"list_isoforms_*"):
    number = file[len(file)-3:]
    
    f_output.write ("python "+base_script+"calculate_score_genome.py calculate_scores --file_uniprots "+ file+
           " --path_file_coordinates "+os.path.join(base_data_source,"coordinates_aminoacids.tsv.gz")+
           " --dict_amino_acids "+os.path.join(base_data_source,"aas_degrons_isoforms.json")+
           " --file_output "+os.path.join(output,"output_scores","output_score_"+str(number)+".tsv"+"\n"))
f_output.close()
           
'''
@cli.command(context_settings={'help_option_names': ['-h', '--help'], 'max_content_width': 200})
@click.option('--file_uniprots', help='file with the uniprots to be processed')
@click.option('--path_file_coordinates', help='Path to the dataframe of the coordinates of each amino-acid')
@click.option('--dict_amino_acids', help='Ditionary with all the amino acids involved in degrons')
@click.option('--file_output', help='output file')
'''
