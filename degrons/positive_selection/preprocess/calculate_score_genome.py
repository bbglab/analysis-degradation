# Calculate the Functional score for a set of input uniprot
import pandas as pd
import json
import sys
import calculate_score_mutations_isoform_normalized as sc
import click

def read_uniprots(file_input):
    # Given the file returns a list of uniprots to be calculated
    list_uniprots = []
    with open(file_input) as f:
        for line in f:
            line = line.rstrip()
            list_uniprots.append(line)
    return list_uniprots

def translate(seq,codon_table):
    '''
    Translate a codon into its amino_acid
    '''
    seq = seq.lower().replace('\n', '').replace(' ', '')
    peptide = ''

    for i in range(0, len(seq), 3):
        codon = seq[i: i+3]
        amino_acid = codon_table.get(codon, '*')
        if amino_acid != '*' or True:
            peptide += amino_acid
        else:
            break

    return peptide
def get_others(nt):
    '''
    get all posible mutations of a nucleotide
    '''

    if nt.upper() == "A":
        return ["C","T","G"]
    elif nt.upper() == "C":
        return ["G","T","A"]
    elif nt.upper() == "T":
        return ["G","C","A"]
    elif nt.upper() == "G":
        return ["T","C","A"]
    else:
        return []

def get_changes_codon(codon):
    '''
    get all posible changes of a codon
    '''
    codon_mt = list(codon)
    pos = 0
    list_codons = []
    for nt in codon:
        lst_others = get_others(nt)
        for nt_a in lst_others:
            codon_mt[pos]=nt_a
            list_codons.append("".join(codon_mt))
            codon_mt = list(codon)
        pos = pos +1
    return list_codons

def get_info(entry,position,df_coordinates):
    '''
    returns a dataframe with the information of an amino acid 
    '''
    v =  df_coordinates[(df_coordinates["Entry_Isoform"]==entry)&(df_coordinates["Position"]==int(position))][["chr","STRAND","coordinates","AA","CODON"]].values
    if len(v) > 0:
        return v[0]
    else:
        #print (entry,position)
        return None

def complement(value):
    '''
    obtain the complementary
    '''
    if value == "C":
        return "G"
    elif value == "G":
        return "C"
    elif value =="A":
        return "T"
    elif value == "T":
        return "A"
    return value

@click.group()
def cli():
    pass

    
@cli.command(context_settings={'help_option_names': ['-h', '--help'], 'max_content_width': 200})
@click.option('--file_uniprots', help='file with the uniprots to be processed')
@click.option('--path_file_coordinates', help='Path to the dataframe of the coordinates of each amino-acid')
@click.option('--dict_amino_acids', help='Ditionary with all the amino acids involved in degrons')
@click.option('--file_output', help='output file')
def calculate_scores(file_uniprots, path_file_coordinates, dict_amino_acids, file_output):
    '''
    For a given number of uniprots from file_uniprots calculates the degScore score of all possible SNVs. 
    '''

    # Initialization
    list_uniprots = read_uniprots(file_uniprots)
    sca = sc.Score_Calculator()
    df_coordinates = pd.read_csv(path_file_coordinates,sep="\t",compression="gzip")
    d_query =   json.load( open( dict_amino_acids, "r"))
    bases = ['t', 'c', 'a', 'g']
    codons = [a+b+c for a in bases for b in bases for c in bases]
    amino_acids = 'FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG'
    codon_table = dict(zip(codons, amino_acids))
    
    list_data = []
    for uniprot in list_uniprots:
        positions = d_query[uniprot] # Give me the amino acids to be queried
        for position in positions:
            v = get_info(uniprot,position,df_coordinates) # check coordinates
            if v is None:
                continue
            (chr_,strand,coordinates,wt_aa,codon)= v
            changes = get_changes_codon(codon,)

            if strand == "+":
                mt_aas = [translate(change,codon_table) for change in changes]
            else: # reverse
                mut_ass = [translate(change[::-1],codon_table) for change in changes]
            i = 0
            for change in changes:
                pos_change = i // 3
                coordinate = coordinates.split(",")[pos_change]
                if strand =="+":       
                    aa = translate(change,codon_table)
                else:
                    aa = translate(change[::-1],codon_table)

                mutation = wt_aa+str(position)+aa
                l_values = sca.get_score_list_mutations_aslist([[uniprot,mutation]])
                for value in l_values:
                    if strand == "+":
                        c_list = [chr_,coordinate,codon[pos_change],change[pos_change],codon,change] + value
                    else:
                        c_list = [chr_,coordinate,complement(codon[pos_change]),complement(change[pos_change]),codon,change] + value
                    list_data.append(c_list)


                i = i +1
    df_muts = pd.DataFrame(list_data,columns=["chr","Coordinate","REF","ALT","REF_CODON","ALT_CODON","DEGRON","START","END","MUTATION","Type_Alteration","Entry","PTM","Altered_Degron","SCORE"])
    df_muts.drop_duplicates(inplace=True)
    df_muts.to_csv(file_output,sep="\t",index=False)

    
if __name__ == '__main__':

    cli()



