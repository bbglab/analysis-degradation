import os
import glob
import re

dir_base = "/workspace/projects/ubiquitins/codereview/degradation/"
dataset = "tcga"
kmer = "penta"
degron ="denovo"
dir_mutations = os.path.join(dir_base,"data","mutations",dataset)
if degron == "predicted":
    path_regions_degrons = os.path.join(dir_base,"data","coordinates_degrons_formatted.tsv.gz")
else:
    path_regions_degrons = os.path.join(dir_base,"data","coordinates_denovodegrons_formatted.tsv.gz")
path_signatures = os.path.join(dir_base,"external","signatures",kmer)
path_regions_cds = os.path.join(dir_base,"external/cds.regions.tsv.gz")
path_output = os.path.join(dir_base,"data","output_methods_selection","output_smdeg_degrons_paper"+"_"+dataset+"_"+degron+"_"+kmer+"/")
path_conf = "/workspace/users/fmartinez/ubiquitins_analysis/smdeg/smdeg/smdeg.conf.template"
f = open(os.path.join(dir_base,"data","output_methods_selection","commands_smdeg_degrons"+"_"+dataset+"_"+degron+"_"+kmer+".sh"),'w')
f.write("[pre]\n")
f.write('. "/home/$USER/miniconda3/etc/profile.d/conda.sh"\n')
f.write('conda activate smdeg \n')
f.write("[params]\n")
cores = "24"
memory = "64G"
f.write(f"cores = {cores}\n")
f.write(f"memory = {memory}\n")
f.write("[jobs]\n")
print ("Reading data from: "+dir_mutations+"/*.tsv.gz")
for file in glob.glob(dir_mutations+"/*.tsv.gz"):

    m = re.search('([A-Z]+).tsv.gz', file)
    if m:
        cancer_type = m.group(1)
        f.write ("smdeg -m "+file+" -o "+path_output+" -r "+path_regions_degrons + " -s "
                 +os.path.join(path_signatures,cancer_type+".json")+ " -c "+path_conf + " -e " + path_regions_cds + " \n")

f.close()

