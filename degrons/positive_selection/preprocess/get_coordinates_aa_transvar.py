'''
script to generate transvar coordinates from a list of uniprots
'''

import sys
import json
import subprocess as sb

# arguments
file_up = sys.argv[1] #	file with the uniprots
job_num = sys.argv[2] #	job number

# paths
output_dir = "/workspace/projects/ubiquitins/annotation_transvar/output_annotation/"
dict_positions =  json.load( open( "/workspace/projects/ubiquitins/codereview/degradation/data/aas_degrons_up.json", "r" ))

# Function to read the uniprots
def read_uniprots(file_up):
    f = open(file_up)
    l = list()
    for line in f:
        line = line.rstrip()
        l.append(line)
    return l

l_uniprots = read_uniprots(sys.argv[1])	# read the list	of uniprot 
cmd = "singularity run -c -e -B /workspace/datasets/transvar/homo_sapiens/87_GRCh37:/data /workspace/soft/singularity/transvar/transvar-2.4.1.simg"
for uniprot in l_uniprots:
    if uniprot in dict_positions:
        for aa in dict_positions[uniprot]:
            sb.call(cmd+" panno --ccds -i "+uniprot+":"+str(aa)+" --uniprot >> "+output_dir+"info_aas_"+str(job_num)+".txt",shell=True)
