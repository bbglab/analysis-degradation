import os
import glob
import re

dir_base = "/workspace/projects/ubiquitins/codereview/degradation/"
dataset = "ccle"
kmer = "penta"
dir_mutations = os.path.join(dir_base,"data","mutations",dataset)
path_regions = os.path.join(dir_base,"data","coordinates_degrons_formatted.tsv.gz")
path_signatures = os.path.join(dir_base,"external","signatures",kmer)
path_output = os.path.join(dir_base,"data","output_methods_selection","output_fmdeg_degrons_paper"+"_"+dataset+"_"+kmer+"/")
path_conf = "/workspace/users/fmartinez/ubiquitins_analysis/fmdeg/fmdeg/fmdeg.conf"
f = open(os.path.join(dir_base,"data","output_methods_selection","commands_fmdeg_degrons_"+dataset+"_"+kmer+".sh"),'w')
f.write("[pre]\n")
f.write('. "/home/$USER/miniconda3/etc/profile.d/conda.sh"\n')
f.write('conda activate smdeg \n')
f.write("[params]\n")
cores = "24"
memory = "64G"
f.write(f"cores = {cores}\n")
f.write(f"memory = {memory}\n")
f.write("[jobs]\n")
print ("Reading data from: "+dir_mutations+"/*.tsv.gz")
for file in glob.glob(dir_mutations+"/*.tsv.gz"):

    m = re.search('([A-Z]+).tsv.gz', file)
    if m:
        cancer_type = m.group(1)
        f.write ("fmdeg -m "+file+" -o "+path_output+" -e "+path_regions + " -s "
                 +os.path.join(path_signatures,cancer_type+".json")+ " -c "+path_conf + " \n")

f.close()

