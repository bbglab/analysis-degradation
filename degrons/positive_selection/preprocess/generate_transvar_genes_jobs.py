'''
Script that generates the jobs to run all uniprots of split files

'''

import glob
import os

base = "/workspace/projects/ubiquitins/annotation_transvar/"
dir_base = os.path.join(base,"split_uniprots/")
path_script = os.path.join(base,"get_coordinates_")
print (dir_base)
list_run = []
f_output = open(os.path.join(base,"commands_gene_transvar.sh"),'w')
f_output.write("[pre]\n")
f_output.write('. "/home/$USER/miniconda3/etc/profile.d/conda.sh"\n')
f_output.write("conda activate transvar\n")
f_output.write("[params]\n")
cores = "1"
memory = "16"
# parameters for all the jobs
f_output.write(f"cores = {cores}\n")
f_output.write(f"memory = {memory}\n")
f_output.write("[jobs]\n")
for file in glob.glob(dir_base+"list_uniprots_*"):
    number = file[len(file)-3:len(file)]
    f_output.write("python /workspace/projects/ubiquitins/codereview/degradation/create_dataframes/positive_selection/degrons/code/get_coordinates_gene_transvar.py "+ file + " "+ str(number)+"\n")

    