import pandas as pd
import numpy as np
import os



path_base = "../"
path_ubi_sites = os.path.join(path_base,"data","ubiquitination_sites_human.tsv.gz")
path_sequences = os.path.join(path_base,"data","sequences_isoforms.tsv")


def get_position(row):

    for mut in row["Protein_Mutations"].split(","):
        if len(mut)>1:
            return mut[1:-1]
def concat(grp):

        l = []
        for value in grp:
            l.append(str(value))
        return ",".join(l)

def find_hit(row,limit,init):

        list_positions = str(row["Position"]).split(",")
        if str(row["Position"]) == "nan":
            return np.nan
        output = []
        degron_range = range(int(row["START"])-limit, int(row["START"]) -init+1)
        degron_range = list(degron_range) + list(range(int(row["END"])+init, int(row["END"]) + limit+1))
        degron_s = set(degron_range)
        for i in range(0, len(list_positions)):
            domain_range = range(int(list_positions[i]), int(list_positions[i]) + 1)
            if len(degron_s.intersection(domain_range)) > 0:
                output.append(list_positions[i])
        if len(output) == 0:
            return np.nan
        return ",".join(output)

def find_lysines_sequence(row,limit,init):

    positions_explore = list(range(int(row["START"])-limit,int(row["START"])-init+1))
    positions_explore = positions_explore + list(range(int(row["END"])+init,int(row["END"])+limit+1))
    seq = row["Sequence"]
    output = []
    for pos in positions_explore:
        if pos <=0:
            continue
        try:
            if seq[pos-1] == "K":
                output.append(str(pos))
        except IndexError:
            continue

    if len(output) == 0:
        return np.nan
    return ",".join(output)

def get_first(grp):
    return list(grp)[0]


def find_sorrounding_lysines(df,limit=11,init=1):
    '''
    Function to find sorrounding lysines in the +/- limit positions
    :param df:
    :param limit: limit to search (default 11 amino acids)
    :param init: start (default next amino acid)
    :return:
    '''

    try:
        if "MOD_RSD" in df.columns.values:
            df.drop(["MOD_RSD"], axis=1, inplace=True)
        if "Position" in df.columns.values:
            df.drop(["Position"], axis=1, inplace=True)
        if "ub_lysines" in df.columns.values:
            df.drop(["ub_lysines"], axis=1, inplace=True)
        if "nflanking_ub_lysines" in df.columns.values:
            df.drop(["nflanking_ub_lysines"], axis=1, inplace=True)
        if "any_lysines" in df.columns.values:
            df.drop(["any_lysines"], axis=1, inplace=True)
        if "nflanking_lysines" in df.columns.values:
            df.drop(["nflanking_lysines"], axis=1, inplace=True)
    except ValueError:
        print ("Creating the columns")

    # Annotate ubiquitination sites

    df_ubi_sites = pd.read_csv(path_ubi_sites,sep="\t",compression="gzip")
    df_ubi_sites.rename(columns={"ACC_ID":"Entry"},inplace=True)
    df_ubi_sites = df_ubi_sites.groupby("Entry",as_index=False).agg({"MOD_RSD":concat,"Position":concat})
    df = pd.merge(left=df,right=df_ubi_sites[["MOD_RSD", "Entry", "Position"]].drop_duplicates(),how="left")
    df["ub_lysines"] = df.apply(lambda row: find_hit(row,limit,init),axis=1)
    df["nflanking_ub_lysines"] =  df.apply(lambda row: 0 if str(row["ub_lysines"])=="nan" else len(str(row["ub_lysines"]).split(",")),axis=1)

    # Annotate lysines (either ubiquitinated or not)

    df_sequences = pd.read_csv(path_sequences, sep="\t")
    df_sequences=df_sequences.groupby(["Entry_Isoform"],as_index=False).agg({"Sequence":get_first}) # Make only one sequence per isoform
    df_query = pd.merge(df_sequences,df[["Entry_Isoform","START","END","DEGRON"]].drop_duplicates())
    df_query["any_lysines"] = df_query.apply(lambda row: find_lysines_sequence(row,limit,init),axis=1)
    df_query["nflanking_lysines"] =  df_query.apply(lambda row: 0 if str(row["any_lysines"])=="nan" else len(str(row["any_lysines"]).split(",")),axis=1)

    df = pd.merge(left=df, right=df_query[["any_lysines","nflanking_lysines", "Entry_Isoform","START","END","DEGRON"]].drop_duplicates(), how="left")

    return df


