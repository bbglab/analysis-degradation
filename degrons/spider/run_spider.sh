#!/bin/bash
# the blastpgp and NR database
NR=//workspace/users/fmartinez/SPIDER/uniref50/nr.50 # Path to uniref50
xdir=$PWD # Path to script path
arg=$1
beforedot=${arg%.*}
echo $beforedot
echo "psiblast -db $NR -num_iterations 3 -num_alignments 1 -num_threads 4 -query $beforedot.fa -out_ascii_pssm $beforedot.pssm"
psiblast -db $NR -num_iterations 3 -num_alignments 1 -num_threads 4 -query $beforedot.fa -out_ascii_pssm $beforedot.pssm
$xdir/pred_pssm.py $beforedot.pssm

