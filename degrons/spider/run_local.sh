#!/bin/bash
# the blastpgp and NR database
blastpgp=/home/fran/anaconda2/bin/psiblast
NR=/home/fran/Documents/ubiquitin/data/dregons/predictions/SS_PRED/psipred/uniref50/nr.50
if [ ! -f $blastpgp ]; then echo "blastpgp is not correctly set"; exit 1; fi
#if [ ! -f $NR.pal ]; then echo "NR database is required"; exit 1; fi

########
xdir=/home/fran/Documents/ubiquitin/data/dregons/predictions/Surface_Acc/SPIDER2_local/misc/
if [ $# -lt 1 ]; then
	echo "usage: $0 *"
	echo "   required: pro1.seq or pro1.pssm"
	exit 1
fi

for seq1 in $*; do
	pro1=$(basename $(basename $seq1 .seq) .pssm)
#	[ -f $pro1.spd3 ] && continue
	if [ ! -f $pro1.pssm ]; then
		echo "psiblast -db $NR -num_iterations 3 -num_alignments 1 -num_threads 16 -query $pro1.seq -out_ascii_pssm $pro1.pssm"

		psiblast -db $NR -num_iterations 3 -num_alignments 1 -num_threads 16 -query $pro1.seq -out_ascii_pssm $pro1.pssm
#		$blastpgp -d $NR -j 3 -b 1 -a 16 -i $pro1.seq -Q $pro1.pssm > /dev/null
	fi

	$xdir/pred_pssm.py $pro1.pssm
done

