import pandas as pd
import numpy as np
import os


def excess_muts(n_obs, omega):
    """
    n_obs: int: number of observed mutations of a kind
    omega: float: applicable dnds estimate
    """
    if (n_obs == 0) or np.isnan(n_obs) or np.isnan(omega):
        return n_obs
    elif 0 <= omega <= 1:
        return 0
    elif omega > 1:
        return round((omega - 1) * n_obs / omega)


def excess_rate(n_obs, omega):
    """
    n_obs: int: number of observed mutations of a kind
    omega: float: applicable dnds estimate
    """
    if (n_obs == 0) or np.isnan(n_obs) or np.isnan(omega):
        return 0
    elif 0 <= omega <= 1:
        return 0
    elif omega > 1:
        return (omega - 1) / omega


def add_excess(df):
    df['excess_mis'] = df.apply(lambda v: excess_muts(v['n_mis'], v['wmis_cv']), axis=1)
    df['excess_non'] = df.apply(lambda v: excess_muts(v['n_non'], v['wnon_cv']), axis=1)
    df['excess_spl'] = df.apply(lambda v: excess_muts(v['n_spl'], v['wspl_cv']), axis=1)
    df['excess_rate_mis'] = df.apply(lambda v: excess_rate(v['n_mis'], v['wmis_cv']), axis=1)
    df['excess_rate_non'] = df.apply(lambda v: excess_rate(v['n_non'], v['wnon_cv']), axis=1)
    df['excess_rate_spl'] = df.apply(lambda v: excess_rate(v['n_spl'], v['wspl_cv']), axis=1)

    return df


def append_excess(path_primary_dnds, path_mets_dnds, df, column_cohort ="COHORT"):
    '''
    :param path_primary_dnds: path_to_primaries dnds .out.gz
    :param path_mets_dnds:  path_to_mets dnds .out.gz
    :param df: input dataframe
    :param column_cohort: name of the cohort column
    :return: the dataframe with the columns added
    '''
    dnds_folder_primary = path_primary_dnds
    dnds_folder_mets = path_mets_dnds
    dg_list = []
    cohorts = df[column_cohort].unique()
    for cohort_orig in cohorts:
        if "HARWIG" in cohort_orig:
            dnds_folder = dnds_folder_mets

            cohort_search = "_".join(cohort_orig.split("_")[2:])
        else:

            dnds_folder = dnds_folder_primary
            cohort_search = cohort_orig
        f_name = '{0}.out.gz'.format(cohort_search)
        output = os.path.join(dnds_folder, f_name)
        dg = pd.read_csv(output, sep='\t')
        dg = dg[['gene_name', 'wmis_cv', 'wnon_cv', 'wspl_cv', 'n_mis', 'n_syn', 'n_spl', 'n_non']].copy()
        dg['COHORT'] = cohort_orig
        dg_list.append(dg)
    dnds_df = pd.concat(dg_list)
    dh = pd.merge(df, dnds_df, left_on=['SYMBOL', column_cohort],right_on=['gene_name', column_cohort],how="left")
    dh_with_excess = add_excess(dh)
    return dh_with_excess

def get_all_excess(path_primary_dnds, path_mets_dnds, df, column_cohort ="COHORT"):
    '''
    Get excess for all genes in df no matter if they are drivers or no
    :param path_primary_dnds: path_to_primaries dnds .out.gz
    :param path_mets_dnds:  path_to_mets dnds .out.gz
    :param df: input dataframe
    :param column_cohort: name of the cohort column
    :return: the dataframe with the columns added
    '''
    dnds_folder_primary = path_primary_dnds
    dnds_folder_mets = path_mets_dnds
    dg_list = []
    genes = df["SYMBOL"].unique()
    cohorts = df[column_cohort].unique()
    for cohort_orig in cohorts:
        if "HARWIG" in cohort_orig:
            dnds_folder = dnds_folder_mets

            cohort_search = "_".join(cohort_orig.split("_")[2:])
        else:

            dnds_folder = dnds_folder_primary
            cohort_search = cohort_orig
        f_name = '{0}.out.gz'.format(cohort_search)
        output = os.path.join(dnds_folder, f_name)
        dg = pd.read_csv(output, sep='\t')
        dg = dg[['gene_name', 'wmis_cv', 'wnon_cv', 'wspl_cv', 'n_mis', 'n_syn', 'n_spl', 'n_non']].copy()
        dg = dg[dg["gene_name"].isin(genes)]
        dg['COHORT'] = cohort_orig
        dg_list.append(dg)
    dnds_df = pd.concat(dg_list)
    dh_with_excess = add_excess(dnds_df)
    return dh_with_excess