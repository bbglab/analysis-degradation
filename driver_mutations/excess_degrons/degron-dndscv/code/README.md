# degron-dndscv

To run:
- make sure your working environment complies with dependencies.txt
- set up the right folder paths for input data in dnds_degron.py and run_dnds.py preambles;
- set up the right path for output in the __main__ of run_dnds.py;
- $ python run_dnds.py
