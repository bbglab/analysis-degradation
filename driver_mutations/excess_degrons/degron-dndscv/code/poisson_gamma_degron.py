import math
import numpy as np
import scipy.stats
from itertools import product
from scipy.special import gamma
from statsmodels.base.model import GenericLikelihoodModel

from utils import mutkeygen, sumdict 


class PoissonGamma(GenericLikelihoodModel):

    """Attributes and methods to model and fit the likelihood of substitution counts by gene as a Poisson-Gamma"""

    def __init__(self, mutcount, sitecount, relfreq, mu, a, exog=None, **kwds):
        """
        Args:
            mutcount: dict of mutation counts per consequence type and signature key
            relfreq: relative frequency associated to signature keys
            sitecount: dict of counts of sites per consequence type and signature key
            mu: estimate mean of gamma distribution modelling t
            a: over-dispersion parameter
        """

        self.mutcount = mutcount
        self.sitecount = sitecount
        self.relfreq   = relfreq
        self.mu = mu
        self.a = a
        self.csqn_types  = ['syn', 'mis', 'other']

        # endog, exog requirements of parent class GenericLikelihoodModel
        endog = []
        for csqn in self.csqn_types:
            for key in mutkeygen():
                endog.append(self.mutcount[key][csqn])
        if exog is None:
            exog = np.zeros_like(endog)

        # parent class __init__
        super(GenericLikelihoodModel, self).__init__(endog, exog, **kwds)
        self.endog_as_float = self.endog.astype('float').tolist()

        # starting parameters
        self.start_params_dict = dict(zip(self.csqn_types, np.zeros(len(self.csqn_types))))
        self.start_params = np.zeros(len(self.csqn_types))
        self.fixed_params = None

    def alpha_beta(self, fixed_index=None):
        """
        Returns:
            alpha and beta parameter estimates of the gamma distribution model that governs the
            background mutation rate t.
        """
        if fixed_index is None:
            fixed_index = [0]

        alpha = 1 / self.a
        exp_neutral = 0
        for i in fixed_index:
            csqn = self.csqn_types[i]
            for key in mutkeygen():
                exp_neutral += self.relfreq[key] * self.sitecount[key].get(csqn, 0)
        beta = exp_neutral * alpha / self.mu
        return alpha, beta

    def mle_t(self, alpha, beta, fixed_index=None):
        """
        MLE estimation of t
        """

        if fixed_index is None:
            fixed_index = [0]

        n_neutral = 0
        for i in fixed_index:
            csqn = self.csqn_types[i]
            n_neutral += float(sum([self.mutcount[k][csqn] for k in mutkeygen()]))
        return (n_neutral + alpha - 1) / (beta * (1 + self.a * self.mu))

    def loglike(self, params):
        """
        Args:
            params: array-like: params encode the values for t and the omega components
        Returns:
            minus log likelihood of the model given the parameters
        """
        fixed_index = None
        if self.fixed_params is not None:
            params = self.expandparams(params)
            isfixedmask = ~np.isnan(self.fixed_params)
            all_indices = np.arange(len(self.csqn_types))
            fixed_index = all_indices[isfixedmask]
        alpha, beta = self.alpha_beta(fixed_index=fixed_index)
        t = self.mle_t(alpha, beta, fixed_index=fixed_index)
        omega_params = dict(zip(self.csqn_types, params))
        return loglike(self.endog_as_float, t, omega_params, alpha, beta, self.sitecount, self.relfreq, self.csqn_types)

    def fit(self, maxiter=200, **kwds):
        """
        Args:
            maxiter: max number of iterations
        Returns:
            MLE fit for the parameters governing the model (inherited method). It employs the
            optimization method attribute.
        """

        return super(GenericLikelihoodModel, self).fit(start_params=self.start_params, maxiter=maxiter,
                                                       method='nm', skip_hessian=True,
                                                       xtol=1e-4, ftol=1e-4, **kwds)

    def positive_selection(self, verbose=False, free_csqn=None):

        n = len(self.csqn_types)

        # H0 => llkd L0
        # w_syn = w_mis = 1, w_other = free

        fixed = np.nan * np.ones(n)
        fixed[0] = 0
        fixed[1] = 0

        self.fixed_params = fixed
        self.fixed_paramsmask = np.isnan(fixed)
        self.start_params = self.start_params[self.fixed_paramsmask]
        self.res = self.fit(disp=verbose)
        self.L0 = self.loglike(self.res.params)

        # restore default params
        self.fixed_params = None
        self.start_params = np.zeros(len(self.csqn_types))

        # H1 => llkd L1
        # free_csqn determines which w parameters are free

        if free_csqn is None:
            self.res = self.fit(disp=verbose)
            self.L1 = self.loglike(self.res.params)
            w_alt = list(map(lambda x: np.exp(x), self.res.params))
            dof = len(fixed[fixed == 0])
        else:
            fixed = np.nan * np.ones(n)
            for i in range(n):
                if i not in list(map(self.csqn_types.index, free_csqn)):
                    fixed[i] = 0
            self.fixed_params = fixed
            self.fixed_paramsmask = np.isnan(fixed)
            self.start_params = self.start_params[self.fixed_paramsmask]
            self.res = self.fit(disp=verbose)
            self.L1 = self.loglike(self.res.params)
            w_alt = list(map(lambda x: np.exp(x), self.res.params))
            dof = len(free_csqn) - 1

        # restore default params

        self.fixed_params = None
        self.start_params = np.zeros(len(self.csqn_types))

        # Compute log ratio and chi-squared statistics

        log_ratio = self.L1 - self.L0
        chi = 2 * log_ratio
        p = scipy.stats.chi2.sf(chi, dof)
        test = (log_ratio, w_alt, p)

        # keep results in a dataframe

        n_elem = sumdict(self.mutcount)
        llkd   = test[0]
        w      = test[1][0]
        p      = test[2]

        res = {'nmis': n_elem,
               'wmis': w,
               'llkd': llkd,
               'p'   : p}

        return res


def loglike(endog_as_float, t, params, alpha, beta, sitecount, relfreq, csqn_types):

    omega = {}
    for key, val in params.items():
        omega[key] = np.exp(val)

    log_lkhd = (alpha * np.log(beta)) - np.log(gamma(alpha)) + ((alpha - 1) * np.log(t)) - (beta * t)

    for j, key in enumerate(mutkeygen()):

        sc = sitecount[key]
        rf = relfreq[key]

        k = j
        for csqn in csqn_types:

            sc_csqn = sc[csqn]
            if sc_csqn != 0:

                w = omega[csqn] if csqn != 'syn' else 1.0
                lambda_ = t * rf * sc_csqn * w

                if lambda_ > 0:

                    n = endog_as_float[k]
                    if n == 0.0:
                        log_lkhd -= lambda_
                    elif n == 1.0:
                        log_lkhd += np.log(lambda_) - lambda_
                    else:
                        log_lkhd += n * np.log(lambda_) - lambda_ - math.log(math.factorial(n))
            k += 192

    return log_lkhd
