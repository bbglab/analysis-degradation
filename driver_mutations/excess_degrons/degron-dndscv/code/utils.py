"""Utils used by dnds_degron"""
from collections import defaultdict
from itertools import product
import pandas as pd
import numpy as np 

CB = dict(zip('ACGT', 'ACGT'[::-1]))
csqn_dict = {'synonymous_variant'     : 'syn',
             'missense_variant'       : 'mis',
             'stop_gained'            : 'other',
             'splice_acceptor_variant': 'other',
             'splice_donor_variant'   : 'other'}

def mutkeygen():
    subs = [a + b for a in list('ACGT') for b in list('ACGT') if a != b]
    for s in sorted(subs):
        for c in product('ACGT', repeat=2):
            yield c[0] + s[0] + c[1] + '>' + s[1]


def maf2profile(maf):
    makeup = {k: 0 for k in mutkeygen()}
    for ind, row in maf.iterrows():
        ref, mut   = row['ref'], row['mut']
        ref3, mut3 = row['ref3_cod'], row['mut3_cod']
        if (ref in set('ACGT')) and (mut in set('ACGT')) and (ref != mut):
            ctxt = ref3 + '>' + mut3[1]
            makeup[ctxt] += 1
    return makeup


def normalize(profile, triplets):
    nprofile = {k: profile[k] / triplets[k[:3]] for k in profile}
    total = sum(list(nprofile.values()))
    return {k: nprofile[k] / total for k in nprofile}


def relative(profile):
    """relative with respect to TTT>G"""
    return np.array([profile[k] / profile['TTT>G'] for k in mutkeygen()])


def all_contexts(seq):
    res = {k: 0 for k in mutkeygen()}
    for i in range(1, len(seq) - 1):
        triplet = seq[i-1: i+2]
        for b in 'ACGT':
            if b != triplet[1]:
                res[triplet + '>' + b] += 1
        arr = []
        for k in mutkeygen():
            arr.append(res[k])
    return np.array(arr)


def sumdict(d1, csqn='mis'):
    s = 0
    for k in d1:
        for c in d1[k]:
            if c == csqn:
                s += d1[k][c]
    return s


def diff_count(d1, d2):
    """computes the diff dict d1 - d2"""

    res = d1.copy()
    for k in d2:
        for c in d2[k]:
            res[k][c] = max(0, d1[k][c] - d2[k][c])
    return res


def transform(degroncount):
    degron_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
    for degron in degroncount:
        for t, u in degroncount[degron]['counts'].items():
            for key, v in degroncount[degron]['counts'][t].items():
                for c, w in degroncount[degron]['counts'][t][key].items():
                    if c in csqn_dict:
                        degron_dict[degron][key[1:-1]][csqn_dict[c]] = w
    return degron_dict
