import os
import gzip
import dill as pickle
import numpy as np
from itertools import product
from collections import defaultdict

from poisson_gamma_degron import PoissonGamma
from utils import mutkeygen, maf2profile, normalize, relative, diff_count


# csqn type vocabulary
csqn_dict1 = {'Synonymous'        : 'syn',
              'Missense'          : 'mis',
              'Nonsense'          : 'other',
              'Essential_Splice'  : 'other'}

csqn_dict2 = {'synonymous_variant'     : 'syn',
              'missense_variant'       : 'mis',
              'stop_gained_variant'    : 'other',
              'splice_acceptor_variant': 'other',
              'splice_donor_variant'   : 'other'}



class dNdSAnalysisDegron:

    def __init__(self, annotmuts, nbglm, degron_sitecount,folder):

        self.annotmuts = annotmuts  # mutation data
        self.nbglm     = nbglm      # dataframe after neg binom regression
        self.degron_sitecount = degron_sitecount
        self.folder = folder
        self.sitecountpath = os.path.join(self.folder, 'site_counts.pickle.gz')
        self.cdspath = os.path.join(self.folder, 'tnt_counts_dict.pickle.gz')
        self.hugo2ensemblpath = os.path.join(self.folder, 'HGNC_ENSEMBL_dict.pickle')

        self.get_sitecount()
        self.get_relfreq()

    def get_sitecount(self):

        with gzip.open(self.sitecountpath, 'rb') as f:
            self.sitecount = pickle.load(f)

    def get_sitecount_elem(self, elem):

        if elem in self.annotmuts['gene'].unique():
            elem_type = 'gene'
        else:
            elem_type = 'degron'

        if elem_type == 'gene':
            sitecount_elem = defaultdict(lambda: defaultdict(int))
            with open(self.hugo2ensemblpath, 'rb') as h:
                hugo2ensembl = pickle.load(h)
            elemensembl = hugo2ensembl[elem]
            geneindex = self.sitecount['gene_index'].index(elemensembl)
            matrix = self.sitecount['matrix'][geneindex,:,:]
            for i, k in enumerate(self.sitecount['key_index']):
                for j, csqn in enumerate(self.sitecount['csqn_index']):
                    sitecount_elem[k][csqn_dict2[csqn]] += matrix[i, j]
            return sitecount_elem

        elif elem_type == 'degron':
            return self.degron_sitecount[elem]

    def get_relfreq(self):

        profile = maf2profile(self.annotmuts)
        with gzip.open(self.cdspath, 'rb') as f:
            cds_counts = pickle.load(f)['cds']
        nprofile = normalize(profile, cds_counts)
        self.relfreq = dict([(k, relative(nprofile)[i]) for i, k in enumerate(mutkeygen())])

    def get_mutcount(self, elem):

        if elem in self.annotmuts['gene'].unique():
            elem_type = 'gene'
        else:
            elem_type = 'degron'

        annotmuts_elem = self.annotmuts.loc[self.annotmuts[elem_type] == elem]
        annotmuts_elem = annotmuts_elem[annotmuts_elem['impact'].isin(csqn_dict1)]
        mutcount = defaultdict(lambda: defaultdict(int))
        for ind, row in annotmuts_elem.iterrows():
            ref, mut   = row['ref'], row['mut']
            ref3, mut3 = row['ref3_cod'], row['mut3_cod']
            impact     = row['impact']
            if (ref in set('ACGT')) and (mut in set('ACGT')) and (ref != mut):
                ctxt = ref3 + '>' + mut3[1]
                mutcount[ctxt][csqn_dict1[impact]] += 1
        return mutcount

    def model(self, elem, degron_complement=True):
        """degron_complement: False => dNdS at gene; True => dNdS at degron complement"""

        gene = elem.split(':')[-1] if ':' in elem else elem
        gmutcount = self.get_mutcount(gene)
        gsitecount = self.get_sitecount_elem(gene)
        gmu, a = tuple(self.nbglm.loc[gene, ['mu', 'a']].values)

        if not degron_complement:
            return PoissonGamma(gmutcount, gsitecount, self.relfreq, gmu, a)
        else:
            assert(':' in elem)
            mutcount = self.get_mutcount(elem)
            sitecount = self.get_sitecount_elem(elem)
            mu, a = tuple(self.nbglm.loc[elem, ['mu', 'a']].values)
            dcmutcount  = diff_count(gmutcount, mutcount)
            dcsitecount = diff_count(gsitecount, sitecount)
            dcmu        = gmu - mu
            return PoissonGamma(dcmutcount, dcsitecount, self.relfreq, dcmu, a)


if __name__ == '__main__':
    pass

