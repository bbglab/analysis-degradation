Result tables keys
==================

degron     : degron ID
gene       : HUGO ID
nmis_gene  : no. missense mutations in entire gene
wmis_gene  : missense dNdS at gene level
llkd_gene  : log-likelihood at MLE at gene level
p_gene     : p-value for test at gene level
nmis_dc    : no. missense mutations in the degron complement
wmis_dc    : missense dNdS at degron complement
llkd_dc    : log-likelihood at MLE at degron complement level 
p_dc       : p-value for test at degron complement level
wmis_degron: missense dNdS inferred for degron
