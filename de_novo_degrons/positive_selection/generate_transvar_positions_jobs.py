'''
Script that generates the jobs to run all uniprots of split files

'''

import glob
import os

base = "/workspace/users/fmartinez/annotation_transvar/"
dir_base = os.path.join(base,"split_uniprots/")
path_script = os.path.join(base,"get_coordinates_")
print (dir_base)
list_run = []
f_output = open(os.path.join(base,"commands_transvar.sh"),'w')
f_output.write("#source activate transvar\n")
for file in glob.glob(dir_base+"list_uniprots_*"):
    number = file[len(file)-3:len(file)]
    print (number)
    f_output.write("python /home/fmartinez/annotation_transvar/get_coordinates_transvar.py "+ file + " "+ str(number)+"\n") # Define your path to the script
