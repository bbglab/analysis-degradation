'''
Script that finds peaks of degron-like regions using a classifier to score how degron-like is sequence motif and a method to find peaks in signals

In this implementation it has been used the classifier using the random training set and the find_peaks implementation of scipy package


'''

import pandas as pd
import os
import numpy as np
from scipy import stats
from sklearn.externals import joblib
from scipy import signal
from calculate_biochemical_properties import calculate_biochemical_properties
import pickle

base="../"
sequences = os.path.join(base,"data","sequences_isoforms.tsv")


# Classifier
covariate_names=["ASA","DSS","F_CONS","COIL","ANCHOR","PHOSPHO_SITES","FLANKING_UB_SITES","HELIX","SHEET","RIG","DOMAIN"] # Labels
covariates =["ASA_SCORE","DSS_SCORE","FCONS_SCORE","COIL","ANCHOR_SCORE","nflanking_ptms","nflanking_ub_lysines","HELIX","STRAND","RIG_SCORE","Domain_pfam"]
response_var = "IS_DEGRON"
#output_classifier_random  = os.path.join(base,"create_dataframes","degrons","data","classifier_random.pickle")
output_classifier_random  = "../data/classifier_random.pickle"
#path_simulated_random = os.path.join(base,"create_dataframes","degrons","data","simulated_degrons.csv.gz")
path_simulated_random = "../data/simulated_degrons.tsv.gz"

def read_sequences():
    def calculate_lenght(row):
        return len(list(row["Sequence"]))

    df_sequences = pd.read_csv(sequences, sep="\t")
    df_sequences["Lenght"] = df_sequences.apply(lambda row: calculate_lenght(row), axis=1)
    return df_sequences

def get_profile_protein(hugo,entry_isoform,lenght,clf,N=3):
    '''
    Function that creates the profile of a protein of interest
    :param hugo: Hugo Symbol
    :param entry_isoform: Uniprot id of the isoform
    :param lenght: lenght of the sequence
    :param clf: classifier
    :return: amino-acid mean value of probability of degron based of windows of N size.
    '''
    l = []
    for i in range(1,lenght-N):
        l.append([hugo,entry_isoform,i,i+N,entry_isoform+" "+"("+str(i)+":"+str(i+N)+")",entry_isoform.split("-")[0]])
    df_query = pd.DataFrame(l,columns=["Hugo_Symbol","Entry_Isoform","START","END","DEGRON","Entry"])
    df_properties_degron_internal = calculate_biochemical_properties(df_query,simulated_path=path_simulated_random,simulations=True,lysines=(3,20),ptms=11)
    p_probs = clf.predict_proba(df_properties_degron_internal[covariates])
    df_properties_degron_internal["Prob_DEGRON"] = [l[1] for l in p_probs]
    df_properties_degron_internal["Prob_RANDOM"] = [l[0] for l in p_probs]
    df_properties_degron_internal["Predicted_Class"] = clf.predict(df_properties_degron_internal[covariates])
    degron_values = []
    for i in range(1,lenght):
        degron_values.append(np.nanmean(df_properties_degron_internal[(df_properties_degron_internal["START"]<=i)&(df_properties_degron_internal["END"]>=i)]["Prob_DEGRON"].values))
    return degron_values

def find_peaks(list_hugos,list_ups,N=10):
    '''
    List of hugos and up ids to be processed
    :param list_hugos: list of hugo symbols
    :param list_ups:  list of uniprot ids
    :return:
    '''
    df_sequences = read_sequences()
    # Calculate lenght of all sequences to be analyzed
    lens = []
    for up in list_ups:
        lens.append(int(df_sequences[df_sequences["Entry_Isoform"] == up]["Lenght"].values[0]))
     # Load the classifier to be used
    clf = joblib.load(output_classifier_random)
    # Iterate over the query data and create the profile for each degron
    d_profile = {}
    for i in range(len(list_ups)):
        hugo = list_hugos[i]
        up = list_ups[i]
        l = lens[i]
        d_profile[(hugo, up)] = get_profile_protein(hugo, up, l, clf, N=N)

    # Find the peaks based of the scipy algorithm
    final_list = []
    d_final = {}
    for up in d_profile.keys():
        peaks = signal.find_peaks(d_profile[up], width=(3, 100), distance=5, height=0.5, prominence=0.0)
        info = peaks[1]
        starts = info["left_ips"]
        ends = info["right_ips"]
        for i in range(len(starts)):
            final_list.append([up[0], up[1], int(starts[i]), int(ends[i]), peaks[0][i], info["peak_heights"][i]])
        d_final[up] = final_list
    df_peaks = pd.DataFrame(final_list,
                            columns=["Hugo_Symbol", "Entry_Isoform", "START_PEAK", "END_PEAK", "POS_TOP", "Prob_TOP"])
    return df_peaks, d_profile



